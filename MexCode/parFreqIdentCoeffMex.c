// Run to compile code:
// mex CC='gcc -fopenmp' COPTIMFLAGS='-O3 -march=native -ffast-math' LDOPTIMFLAGS='' -R2018a -outdir @SFTPlan -ILib/pocketfft MexCode/parFreqIdentCoeffMex.c Lib/pocketfft/pocketfft_prealloc.c

// Description:
// C MEX implementation with OpenMP support of MATLAB function
// FREQUENCYIDENTIFICATION (identify frequencies with largest coefficients) and
// COEFFICIENTESTIMATE (estimate values of coefficients).
// By comparing coefficients with large magnitudes which appear in the 
// constructed measurement matrix multiplied by the Fourier transform of the 
// signal, the frequencies which correspond to these largest or "most energetic" 
// frequencies are repeatedly identified. Only the ones that are identified 
// enough to be useful in a coefficient estimation step are kept.
//
// MATLAB code by Craig Gross, Mar 2020
// C implementation by Toni Volkmer, May-June 2020

#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

#if __STDC_VERSION__ >= 199901L
/* C99 code */
#define C99
  #define STATIC static
  #define INLINE inline
#else
  #define STATIC
  #define INLINE
#endif

#include <complex.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "pocketfft_prealloc.h"

// function [ inverse ] = modularInverse(p, q)
// %MODULARINVERSE- Calculates p^(-1) mod q for relatively prime integers p and q.
// %Algorithm from https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm#Modular_integers.
// %
// % Syntax:  [ inverse ] = modularInverse(p, q)
// %
// % Inputs:
// %    p: Integer.
// %    q: Integer relatively prime to p.
// %
// % Outputs:
// %    inverse: p^(-1) mod q
// %
// % Author: Craig Gross
// % Mar 2020
// inverse = 0; newInverse = 1;
// r = q; newr = p;
// while newr ~= 0
//   quotient = floor(r / newr);
//   temp = inverse;
//   inverse = newInverse; newInverse = temp - quotient * newInverse;
//   temp = r;
//   r = newr; newr = temp - quotient * newr;
// end
// if r > 1
//   error([num2str(p), ' is not invertible modulo ', num2str(q), '.']);
// end
// if inverse < 0
//   inverse = inverse + q;
// end
// end
STATIC long long modularInverse(long long p, long long q)
// implementation requires p,q >= 0
{
  long long inverse = 0, newInverse = 1, r = q, newr = p;

  while (newr != 0)
  {
	long long quotient = r / newr;
    long long temp = inverse;
	inverse = newInverse; newInverse = temp - quotient * newInverse;
	temp = r;
	r = newr; newr = temp - quotient * newr;
  }

  if (r > 1)
    mexErrMsgIdAndTxt("onePrimeMex:modularInverse",
                      "not invertible");
  if (inverse < 0)
	inverse += q;

  return inverse;
}

// function [ omega ] = reconstruct(r1, r2, s1, s2)
// %RECONSTRUCT- Perform the modular reconstruction for only two remainders.
// %The algorithm is justified by the following calculation:
// % omega = r1 mod s1 = r2 mod s2
// %	implies omega = r1 + s1 * j = r2 mod s2
// %	implies j = s1^-1 (r2 - r1) mod s2
// % Note that 0 <= r1 < s1 and 0 <= j < s2 
// %	implies 0 <= omega = r1 + s1 * j < s1 * s2.
// %
// % Syntax:  function [ omega ] = reconstruct(r1, r2, s1, s2)
// %
// % Inputs:
// %    r1: The remainder of omega mod s1. Can be column vector.
// %    r2: The remainder of omega mod s2. Can be column vector of same length 
// %		as r1.
// %    s1: A positive integer.
// %    s2: A positive integer relatively prime to s1.
// %
// % Outputs:
// %    omega: The unique 0<=omega<s1*s2 satisfying omega = r1 mod s1 = r2 mod s2.
// %		If the remainders are column vector, so is omega, with each entry 
// %		having the corresponding remainders in r1 and r2.
// % Author: Craig Gross
// % Mar 2020
// omega = r1 + s1 .* mod(SFTPlan.modularInverse(s1, s2) .* (r2 - r1), s2);
// end
STATIC void reconstruct(long long *omega, long long len_omega, long long *r2, long long s1, long long s2)
// omega and r2 are arrays of length at least len_omega
{
  long long inv = modularInverse(s1, s2);

  for (long long j = 0; j < len_omega; j++)
  {
    long long mod_val = (inv * (r2[j] - omega[j])) % s2;
    // MATLAB mod guarantees result to be >= 0 but C modulo does not
    if (mod_val < 0)
      mod_val += s2;
    omega[j] = omega[j] + s1 * mod_val;
  }
}
  
// function [ omega ] = modularReconstruction(r, s)
// %MODULARRECONSTRUCTION- Calculate omega using only remainders r = omega mod s.
// %We are guaranteed a unique 0 <= omega < prod(s) by the Chinese remainder 
// %theorem. Note that we assume that the integers in s are relatively prime.
// %
// % Syntax:  [ omega ] = modularReconstruction(r, s)
// %
// % Inputs:
// %    r: Row vector of remainders r = omega mod s satisfying 0 <= r < s.
// %		Optionally, r can also be a matrix, where each column r(:, i) 
// %		represents the remainders of different omega mod s(i), and the
// %		function will simply be applied to all elements.
// %    s: Vector of relatively prime positive integers.
// %
// % Outputs:
// %    omega: The unique 0 <= omega < prod(s) satisfying omega = r mod s.
// %		If the r is a matrix, omega is a column vector with as many rows as r,
// %		with each entry having the corresponding remainders given by the 
// %		columns of r.
// %
// % Other m-files required: modularInverse.m
// % Subfunctions: reconstruct
// %
// % Author: Craig Gross
// % Mar 2020
// % Operate incrementally, starting from first two remainders and working up.
// omega = r(:, 1);
// divisor = s(1);
// for i = 2:length(s)
//   omega = reconstruct(omega, r(:, i), divisor, s(i));
//   divisor = divisor * s(i);
// end
// end
STATIC void modularReconstruction(long long *omega, long long len_omega, long long *r, double *s, long long len_s)
// omega is of length at least len_omega
// r is of length len_omega*len_s
// s is of length len_s
{
  long long divisor = len_omega;
  // Operate incrementally, starting from first two remainders and working up.
  for (long long j = 0; j < len_omega; j++)
    omega[j] = j;

  for (long long i = 0; i < len_s; i++)
  {
	reconstruct(omega, len_omega, r+i*len_omega, divisor, s[i]);
	divisor = divisor * s[i];
  }
}

// function [ product, omega ] = onePrime(plan, j, samples)
// %ONEPRIME- Reconstruct all frequencies from one major prime.
// % Syntax:  [ identified, timesIdentified ] = onePrime(plan, j, samples)
// %     or   [ identified, timesIdentified ] = plan.onePrime(j, samples)
// % Inputs:
// %    j: The index of the prime to use. Should be in 1:plan.K
// %    samples: Samples of the signal at the sampling nodes for the plan.
// % Outputs:
// %    identified: The list of all frequencies identified modulo plan.s(j).
// %    timesIdentified: The number of times each frequency in identified
// %		was identified.
// % See also: GENERALFASTMULTIPLY FREQUENCYIDENTIFICATION
// % Author: Craig Gross
// % Apr 2020
// product = zeros(plan.s(j) * (1 + plan.tSum{j}(end)), 1);
// A = samples(1:plan.s(j));
// product(1:plan.s(j)) = (1 / plan.s(j)) * fft(A);
// start = plan.s(j);
// for i = 1:length(plan.t{j})
//   fftSize = plan.s(j) * plan.t{j}(i);
//   A = samples(start + 1: start + fftSize);
//   product(start + 1: start + fftSize) =...
// 		(1 / fftSize) * fft(A);
//   % Move into next part of product
//   start = start + plan.s(j) * plan.t{j}(i);
// end
// 
// h = (0:plan.s(j) - 1)'; % All residues of s(j)
// r = h + 1; % Their locations in product
// divisor = plan.s(j) * prod(plan.t{j}); 
// remainders = zeros(plan.s(j), length(plan.t{j}));
// rbarBase = plan.s(j);
// for i = 1:length(plan.t{j})
//   % All pairs of residues of s(j) and residues of t{j}(i).
//   rbar = rbarBase + plan.s(j) * (0:(plan.t{j}(i) - 1)) + h + 1;
//   % Find the residues of t{j}(i) which match the original element best
//   [~, bmin] = min( abs(product(r) - product(rbar)), [], 2 );
//   remainders(:, i) = mod(plan.s(j) * (bmin - 1) + h, plan.t{j}(i));
// 
//   % Move into the part of product corresponding to t{j}(i + 1)
//   % or s(j + 1) if there are no more t{j} values.
//   rbarBase = rbarBase + plan.s(j) * plan.t{j}(i);
// end
// omega = SFTPlan.modularReconstruction([h, remainders], ...
// 	[plan.s(j), plan.t{j}]);
// 
// % Ensure frequencies are in the canonical interval
// % -ceil(divisor / 2) + 1:floor(divisor / 2)
// % and are weighted the proper number of times.
// omega = repmat(mod(omega - 1 + ceil(divisor / 2), divisor) ...
// 	+ 1 - ceil(divisor / 2), plan.weights(j), 1);
// 
// end
STATIC void onePrime(long long *omega, double _Complex *product, double _Complex *samples_r, double _Complex *samples_rbar, long long s_j, double *t_j, long long len_t_j, double *twid_c2length, double *ch_2cfftlength, double *wal_2ip, double *mem_blue, double *mem_2twsize)
{
  long long i, k;
//  cfft_plan p;
  for (k = 0; k < s_j; k++)
    product[k] = samples_r[k];

//  p = make_cfft_plan(s_j);
//  cfft_forward(p, (double *) product, 1.0/s_j);
//  destroy_cfft_plan(p);
  cfft_plan_prealloc_i p_i;
  make_cfft_plan_prealloc (&p_i, s_j, mem_2twsize, twid_c2length, ch_2cfftlength, wal_2ip, mem_blue);
  cfft_forward_prealloc(&p_i, (double *) product, 1.0/s_j, twid_c2length, ch_2cfftlength, wal_2ip);
  destroy_cfft_plan_prealloc(&p_i);

  long long start = 0;
  for (i = 0; i < len_t_j; i++)
  {
    long long t_j_i = llrint(t_j[i]);
    double _Complex *A = product + start + s_j;
	long long fftSize = s_j * t_j_i;
    for (k = 0; k < fftSize; k++)
      A[k] = samples_rbar[start+k];

//    p = make_cfft_plan(fftSize);
//    cfft_forward(p, (double *) A, 1.0/fftSize);
//    destroy_cfft_plan(p);
    cfft_plan_prealloc_i p_i;
    make_cfft_plan_prealloc (&p_i, fftSize, mem_2twsize, twid_c2length, ch_2cfftlength, wal_2ip, mem_blue);
    cfft_forward_prealloc(&p_i, (double *) A, 1.0/fftSize, twid_c2length, ch_2cfftlength, wal_2ip);
    destroy_cfft_plan_prealloc(&p_i);
    
    //Move into next part of product
    start +=  s_j * t_j_i;
  }

  long long divisor = s_j;
  for (i = 0; i < len_t_j; i++)
    divisor *= llrint(t_j[i]);
  long long *remainders = (long long *) malloc(s_j * len_t_j * sizeof(long long));
  long long rbarBase = s_j;
  for (i = 0; i < len_t_j; i++)
  {
    long long t_j_i = llrint(t_j[i]);
    long long bmin_minus1[s_j];
    double val_bmin_minus1[s_j];
    for (long long r = 0; r < s_j; r++)
    {
      long long rbar_0 = rbarBase + s_j * 0 + r;
      bmin_minus1[r] = 0;
      val_bmin_minus1[r] = cabs(product[r] - product[rbar_0]);
    }
    for (long long k = 1; k < t_j_i; k++)
    {
      long long rbar_pre = rbarBase + s_j * k;
      for (long long r = 0; r < s_j; r++)
      {
        long long rbar = rbar_pre + r;
        double val_cur = cabs(product[r] - product[rbar]);
        if (val_cur < val_bmin_minus1[r])
        {
          bmin_minus1[r] = k;
          val_bmin_minus1[r] = val_cur;
        }
      }
    }
    for (long long r = 0; r < s_j; r++)
      remainders[r  + i*s_j] = (s_j * bmin_minus1[r] + r) % t_j_i;
    
	rbarBase += s_j * t_j_i;
  }

  modularReconstruction(omega, s_j, remainders, t_j, len_t_j);
  
  for (long long r = 0; r < s_j; r++)
  {
    omega[r] = omega[r] % divisor;
    if (omega[r] < -(divisor/2))
      omega[r] += divisor;
    else if (omega[r] > divisor/2)
      omega[r] -= divisor;
  }
  
  free(remainders);
}

// function [ identified ] = frequencyIdentification(plan, product)
// %FREQUENCYIDENTIFICATION- Identify frequencies with largest coefficients.
// %By comparing coefficients with large magnitudes which appear in the 
// %constructed measurement matrix multiplied by the Fourier transform of the 
// %signal, the frequencies which correspond to these largest or "most energetic" 
// %frequencies are repeatedly identified. Only the ones that are identified 
// %enough to be useful in a coefficient estimation step are kept.
// % Syntax:  [ identified ] = frequencyIdentification(plan, product)
// %      or  [ identified ] = plan.frequencyIdentification(product)
// % Inputs:
// %    product: Extended row tensor product measurement matrix multiplied by 
// %		various Fourier transforms of given signal. Should have form calculated 
// %		by GENERALFASTMULTIPLY.
// % Outputs:
// %    identified: Vector of reconstructed frequencies. Only frequencies which 
// %		have been reconstructed more half total number of primes times will be 
// %		returned.
// % See also: GENERALFASTMULTIPLY
// % Author: Craig Gross
// % Mar 2020
// L = plan.sSum(end);
// 
// identified = zeros(1, L);
// timesIdentified = zeros(1, L);
// numIdentified = 0;
// 
// % Below, we consider all residues h of each major prime s(j). If a 
// % significant frequency = h mod s(j), we calculate its remainders modulo each 
// % t{j}(i) by searching over these residues for the entry in the product 
// % closest to the original entry corresponding to h mod s(j). The original 
// % entry is indexed with r and the entries in the product corresponding to 
// % residues of t{j}(i) are indexed with rbar. 
// %
// % See GENERALFASTMULTIPLY for explanation of indexing scheme.
// rbarBase = L;
// for j = 1:plan.K
//   h = (0:plan.s(j) - 1)'; % All residues of s(j)
//   r = plan.sSum(j) + h + 1; % Their locations in product
//   divisor = plan.s(j) * prod(plan.t{j}); 
//   remainders = zeros(plan.s(j), length(plan.t{j}));
//   for i = 1:length(plan.t{j})
//     % All pairs of residues of s(j) and residues of t{j}(i).
//     rbar = rbarBase + plan.s(j) * (0:(plan.t{j}(i) - 1)) + h + 1;
//     % Find the residues of t{j}(i) which match the original element best
//     [~, bmin] = min( abs(product(r) - product(rbar)), [], 2 );
//     remainders(:, i) = mod(plan.s(j) .* (bmin - 1) + h, plan.t{j}(i));
// 
//     % Move into the part of product corresponding to t{j}(i + 1)
//     % or s(j + 1) if there are no more t{j} values.
//     rbarBase = rbarBase + plan.s(j) * plan.t{j}(i);
//   end
//   omega = SFTPlan.modularReconstruction([h, remainders], ...
// 		[plan.s(j), plan.t{j}]);
// 
//   % Ensure frequencies are in the canonical interval
//   % -ceil(divisor / 2) + 1:floor(divisor / 2)
//   omega = mod(omega - 1 + ceil(divisor / 2), divisor) ...
// 		+ 1 - ceil(divisor / 2);
// 
//   % Track how many times each frequency has been found
//   [~, omegaMatch, idMatch] = intersect(omega, identified(1:numIdentified));
//   % Frequencies already found
//   % For repeated primes, we count this identifications weights(j) times
//   timesIdentified(idMatch) = timesIdentified(idMatch) + plan.weights(j);
//   % Frequencies to add to list
//   omegaNotMatch = setdiff(1:plan.s(j), omegaMatch);
//   identified(numIdentified+1:numIdentified+length(omegaNotMatch)) =...
// 		omega(omegaNotMatch);
//   % For repeated primes, we count this identifications weights(j) times
//   timesIdentified(numIdentified+1:numIdentified+length(omegaNotMatch))=...
//     plan.weights(j);
//   numIdentified = numIdentified + length(omegaNotMatch);
// end
// 
// % Need to be identified more than the TOTAL number of primes including repeats
// identified = identified(timesIdentified > sum(plan.weights) / 2);
// end
STATIC void frequencyIdentificationMex2(double _Complex *product_j, long long *omega_j, long long len_s, long long j, double _Complex *samples, double s_j, double sSum_j, double sSum_end, double *t_j, long long len_t_j, double *s_j_times_tSum_j_end_array, double *twid_c2length, double *ch_2cfftlength, double *wal_2ip, double *mem_blue, double *mem_2twsize)
{
  long long r_start = llrint(sSum_j);
  long long rbar_start = llrint(sSum_end);

  for (long long jj = 0; jj < j; jj++)
    rbar_start += llrint(s_j_times_tSum_j_end_array[jj]);

  long long s_j_int = llrint(s_j);

  long long omega_j_int_array[s_j_int];

  onePrime(omega_j_int_array, product_j, samples+r_start, samples+rbar_start, s_j_int, t_j, len_t_j, twid_c2length, ch_2cfftlength, wal_2ip, mem_blue, mem_2twsize);

  long long divisor = s_j_int;
  for (long long k = 0; k < len_t_j; k++)
    divisor *= llrint(t_j[k]);

  long long ceil_divisor_half = (divisor % 2 != 0) ? divisor / 2 + 1 : divisor / 2;

  for (long long k = 0; k < s_j_int; k++)
  {
    long long tmp = omega_j_int_array[k] - 1 + ceil_divisor_half % divisor;
    if (tmp < 0) tmp += divisor;
    omega_j[k] = (tmp + 1 - ceil_divisor_half);
  }
}

STATIC int comp_long_long_a0(const void * e1, const void * e2)
{
  long long *v1 = ((long long *) e1);
  long long *v2 = ((long long *) e2);

  if (v1[0] < v2[0])
    return -1;
  else if (v1[0] > v2[0])
    return 1;
  return 0;
}

STATIC int comp_real_part(const void * e1, const void * e2) 
{
  double _Complex v1 = *((double _Complex *) e1);
  double _Complex v2 = *((double _Complex *) e2);

  if (creal(v1) < creal(v2))
    return -1;
  else if (creal(v1) > creal(v2))
    return 1;
  return 0;
}

STATIC int comp_imag_part(const void * e1, const void * e2) 
{
  double _Complex v1 = *((double _Complex *) e1);
  double _Complex v2 = *((double _Complex *) e2);

  if (cimag(v1) < cimag(v2))
    return -1;
  else if (cimag(v1) > cimag(v2))
    return 1;
  return 0;
}

// function [ x ] = coefficientForOneFreq(plan, omega, product)
// indices = cell(1, plan.K);
// for i = 1:plan.K
// 	% Standard measurement matrix index
// 	r = mod(omega, plan.s(i)) + 1;
// 	% Tensor product indices
// 	rbar = plan.s(i) * (1 +  plan.tSum{i}(1:end - 1)) + ...
// 		mod(omega, plan.s(i) * plan.t{i}) + 1;
// 	% Each prime should be counted weights(i) number of times
// 	indices{i} = repmat([r, rbar], 1, plan.weights(i));
// end
// 
// coeffCell = cell(plan.K, 1);
// for i = 1:plan.K
// 	coeffCell{i} = product{i}(indices{i});
// end
// coeff = cell2mat(coeffCell);
// 
// x = median(real(coeff)) + 1i .* median(imag(coeff));
// end
STATIC double _Complex coefficientForOneFreq(double _Complex **product_array,
        long long len_s, long long *product_lengths, long long omega,
        double *s_array, double **t_array, long long *t_lengths,
        double **tSum_array, double *weights_array)
{
  double result_r, result_i;
  long long coefficients_length = 0;
  for (long long i = 0; i < len_s; i++)
    coefficients_length += (llrint(t_lengths[i])+1) * llrint(weights_array[i]);

  double _Complex * coefficients = (double _Complex *) malloc(coefficients_length * sizeof(double _Complex));

  long long coefficients_counter = 0;
  for (long long i = 0; i < len_s; i++)
  {
    long long weight = llrint(weights_array[i]);
//    long long omega = llrint(identified_array_k);
    long long s_i = llrint(s_array[i]);
    long long r = omega % s_i;
    if (r < 0) r+= s_i;
    for (long long iw = 0; iw < weight; iw++)
      coefficients[coefficients_counter++] = product_array[i][r];

    long long t_lengths_i = llrint(t_lengths[i]);
    for (long long m = 0; m < t_lengths_i; m++)
    {
      long long d2 = s_i * llrint(t_array[i][m]);
      long tmp = omega % d2;
      if (tmp < 0) tmp += d2;
      long long rbar_m = s_i * (1 +  llrint(tSum_array[i][m])) + tmp;

//       if (rbar_m >= product_lengths[i])
//         mexErrMsgIdAndTxt("coefficientsForAllFreqMex:rbarm",
//                           "Invalid rbar_m value");
        
      for (long long iw = 0; iw < weight; iw++)
        coefficients[coefficients_counter++] = product_array[i][rbar_m];
    }
  }

//   if (coefficients_length != coefficients_counter)
//     mexErrMsgIdAndTxt("coefficientsForAllFreqMex:invalidCounter",
//                       "Invalid coefficients counter value");

  qsort(coefficients, coefficients_length, sizeof(double _Complex), comp_real_part);
  if (coefficients_length % 2 == 1)
    result_r = creal(coefficients[coefficients_length/2]);
  else
    result_r = 0.5 * (creal(coefficients[coefficients_length/2-1]) + creal(coefficients[coefficients_length/2]));

  qsort(coefficients, coefficients_length, sizeof(double _Complex), comp_imag_part);
  if (coefficients_length % 2 == 1)
    result_i = cimag(coefficients[coefficients_length/2]);
  else
    result_i = 0.5 * (cimag(coefficients[coefficients_length/2-1]) + cimag(coefficients[coefficients_length/2]));

  free(coefficients);

  return result_r + I * result_i;
}


void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
      
  if (nrhs < 7)
    mexErrMsgIdAndTxt("parFreqIdentCoeffMex:nrhs",
                      "Five inputs required. Usage: [freq, coeff] = parFreqIdentCoeffMex(samples, plan.s, plan.sSum, plan.t, s_j_times_tSum_j_end_array, plan.tSum, plan.weights)");

  if (nlhs != 2)
    mexErrMsgIdAndTxt("parFreqIdentCoeffMex:nlhs",
                      "Two outputs required. Usage: [freq, coeff] = parFreqIdentCoeffMex(samples, plan.s, plan.sSum, plan.t, s_j_times_tSum_j_end_array, plan.tSum, plan.weights)");

  if (!mxIsComplex(prhs[0]) || mxGetClassID(prhs[0])!=mxDOUBLE_CLASS)
    mexErrMsgIdAndTxt("parFreqIdentCoeffMex:samplesDataType",
                      "First input 'samples' must be complex double array");
    
  long long num_samples = mxGetNumberOfElements(prhs[0]);
  double _Complex *samples = (double _Complex*) mxGetComplexDoubles(prhs[0]);
  
  if (mxIsComplex(prhs[1]) || mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgIdAndTxt("parFreqIdentCoeffMex:sDataType",
                      "Second input 'plan.s' must be real double array");
  long long len_s = mxGetNumberOfElements(prhs[1]);
  double *s_array = (double *) mxGetDoubles(prhs[1]);

  if (mxIsComplex(prhs[2]) || mxGetClassID(prhs[2])!=mxDOUBLE_CLASS
          || mxGetNumberOfElements(prhs[2])!=len_s+1)
    mexErrMsgIdAndTxt("parFreqIdentCoeffMex:sSumDataTypeOrLength",
                      "Third input 'plan.sSum' must be real double array of length: length(plan.s)+1");
  double *sSum_array = (double *) mxGetDoubles(prhs[2]);

  if (!mxIsCell(prhs[3]) || mxGetNumberOfElements(prhs[3]) != len_s)
    mexErrMsgIdAndTxt("parFreqIdentCoeffMex:tDataTypeorLength",
                      "Forth input 'plan.t' must be cell array of length: length(plan.s)");
  double **t_array = (double **) malloc(len_s * sizeof(double *));
  long long *t_lengths = (long long *) malloc(len_s * sizeof(long long *));
  for (long long j = 0; j < len_s; j++)
  {
    const mxArray *t_j = mxGetCell(prhs[3], j);
    t_lengths[j] = mxGetNumberOfElements(t_j);
    if (mxIsComplex(t_j) || mxGetClassID(t_j)!=mxDOUBLE_CLASS)
      mexErrMsgIdAndTxt("parFreqIdentCoeffMex:tContentDataTypeOrLength",
                        "Entries of forth input 'plan.t' must be real double array");
    t_array[j] = (double *) mxGetDoubles(t_j);
  }

  if (mxIsComplex(prhs[4]) || mxGetClassID(prhs[4])!=mxDOUBLE_CLASS
          || mxGetNumberOfElements(prhs[4])!=len_s)
    mexErrMsgIdAndTxt("parFreqIdentCoeffMex:arg5",
                      "Fifth input must be real double array of length: length(plan.s)");
  double *s_j_times_tSum_j_end_array = (double *) mxGetDoubles(prhs[4]);

  if (!mxIsCell(prhs[5]) || mxGetNumberOfElements(prhs[5]) != len_s)
  {
    free(t_array);
    free(t_lengths);
    mexErrMsgIdAndTxt("parFreqIdentMex:tSumDataTypeorLength",
                      "Sixth input 'plan.tSum' must be cell array of length: length(plan.s)");
    return;
  }
  double **tSum_array = (double **) malloc(len_s * sizeof(double *));
  for (long long j = 0; j < len_s; j++)
  {
    const mxArray *tSum_j = mxGetCell(prhs[5], j);

    if (mxIsComplex(tSum_j) || mxGetClassID(tSum_j)!=mxDOUBLE_CLASS
            || mxGetNumberOfElements(tSum_j) != t_lengths[j]+1)
    {
      free(t_array);
      free(t_lengths);
      mexErrMsgIdAndTxt("parFreqIdentMex:tSumContentDataTypeOrLength",
                        "Entries of sixth input 'plan.tSum' must be real double array of correct length");
      return;
    }
    tSum_array[j] = (double *) mxGetDoubles(tSum_j);
  }

  if (mxIsComplex(prhs[6]) || mxGetClassID(prhs[6])!=mxDOUBLE_CLASS)
  {
    free(t_array);
    free(t_lengths);
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:weightsDataTypeOrLength",
                      "Seventh input 'plan.weights' must be real double array of length: length(plan.s)");
    return;
  }
  mxGetNumberOfElements(prhs[6]);
  double *weights_array = (double *) mxGetDoubles(prhs[6]);

#ifdef _OPENMP
  if (nrhs >= 8)
  {
    if (mxGetNumberOfElements(prhs[7]) != 1 || lrint(mxGetScalar(prhs[7])) < 1)
      mexErrMsgIdAndTxt("parFreqIdentCoeffMex:nthreads",
                        "Eighth argument repesenting number of threads has to be >= 1.");
    nthreads = lrint(mxGetScalar(prhs[7]));
  }
#endif

  double _Complex **product_data = (double _Complex**) malloc(len_s * sizeof(double _Complex*));
  long long *product_lengths = (long long *) malloc(len_s * sizeof(long long));
  long long **omega_array = (long long**) malloc(len_s * sizeof(long long*));
  for (long long j = 0; j < len_s; j++)
  {
    long long num_samples_j = llrint(s_j_times_tSum_j_end_array[j]) + llrint(s_array[j]);
    product_data[j] = (double _Complex*) malloc(num_samples_j * sizeof(double _Complex));
	product_lengths[j] = num_samples_j;
    omega_array[j] = (long long*) malloc(llrint(s_array[j]) * sizeof(long long));
  }

  long long max_fftsize = 0;
  for (long long j = 0; j < len_s; j++)
    for (long long i = 0; i < t_lengths[j]; i++)
    {
      long long t = llrint(s_array[j]) * llrint(t_array[j][i]);
      if (max_fftsize < t)
        max_fftsize = t;
    }

  printf("nthreads = %d, max_fftsize = %d\n", nthreads, max_fftsize);
  
  long long j;

  #pragma omp parallel num_threads(nthreads) 
  {
    double *twid_c2length = (double *) malloc(6*max_fftsize *sizeof(double));
    double *ch_2cfftlength = (double *) malloc(6*max_fftsize *sizeof(double));
    double *wal_2ip = (double *) malloc(2*max_fftsize *sizeof(double));
    double *mem_blue = (double *) malloc(8*max_fftsize *sizeof(double));
    double *mem_2twsize = (double *) malloc(6*(max_fftsize+NFCT)*sizeof(double));

    #pragma omp for schedule(dynamic,4)
    for (j = 0; j < len_s; j++)
      frequencyIdentificationMex2(product_data[j], omega_array[j], len_s, j, samples, s_array[j], sSum_array[j], sSum_array[len_s], t_array[j], t_lengths[j], s_j_times_tSum_j_end_array, twid_c2length, ch_2cfftlength, wal_2ip, mem_blue, mem_2twsize);

    free(twid_c2length); free(ch_2cfftlength); free(wal_2ip); free(mem_blue); free(mem_2twsize);
  }


//[frequencies, ~, indices] = unique(cell2mat(omega')');
//timesIdentified = accumarray(indices,cell2mat(omega_w'));
//identified = frequencies(timesIdentified > sum(plan.weights) / 2);
  long long *freqs_weights = (long long *) malloc(2 * sSum_array[len_s] * sizeof(long long));
  long long tmp_cnt = 0;
  long long sum_weights = 0;
  for (long long j = 0; j < len_s; j++)
  {
	sum_weights += weights_array[j];
	for (long long i = 0; i < s_array[j]; i++)
	{
	  freqs_weights[2*tmp_cnt] = omega_array[j][i];
	  freqs_weights[2*tmp_cnt+1] = weights_array[j];
	  tmp_cnt++;
	}
  }

  qsort(freqs_weights, tmp_cnt, 2 * sizeof(long long), comp_long_long_a0);

  long long *identified_array = (long long *) malloc(sSum_array[len_s] * sizeof(long long));
  long long len_identified = 0;
  long long last_omega = freqs_weights[2*0];
  long long last_weight_sum = freqs_weights[2*0+1];
  for (long long k = 1; k < tmp_cnt; k++)
  {
	long long cur_omega = freqs_weights[2*k];
	if (cur_omega != last_omega)
	{
      if (last_weight_sum > sum_weights/2)
	    identified_array[len_identified++] = last_omega;
	  last_weight_sum = 0;
	}
	last_omega = cur_omega;
	last_weight_sum += freqs_weights[2*k+1];
  }
  if (last_weight_sum > sum_weights/2)
	identified_array[len_identified++] = last_omega;

  free(freqs_weights);

  plhs[0] = mxCreateDoubleMatrix(1,len_identified,mxREAL);
  double *identified_data = (double *) mxGetDoubles(plhs[0]);
  for (long long k = 0; k < len_identified; k++)
	identified_data[k] = (double) identified_array[k];

  plhs[1] = mxCreateDoubleMatrix(1,len_identified,mxCOMPLEX);
  double _Complex *allcoefficients = (double _Complex*) mxGetComplexDoubles(plhs[1]);

  #pragma omp parallel for num_threads(nthreads) schedule(dynamic,4)
  for (long long k = 0; k < len_identified; k++)
    allcoefficients[k] = coefficientForOneFreq(product_data, len_s, product_lengths, identified_array[k], s_array, t_array, t_lengths, tSum_array, weights_array);


  for (long long j = 0; j < len_s; j++)
  {
    free(product_data[j]);
    free(omega_array[j]);
  }

  free(identified_array);
  free(tSum_array);
  free(omega_array);
  free(product_data);
  free(product_lengths);
  free(t_lengths);
  free(t_array);
}
