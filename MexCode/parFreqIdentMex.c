// Run to compile code:
// mex CC='gcc -fopenmp' COPTIMFLAGS='-O3 -march=native -ffast-math' LDOPTIMFLAGS='' -R2018a -outdir @SFTPlan -Ipocketfft parFreqIdentMex.c pocketfft/pocketfft.c

// Description:
// C MEX implementation with OpenMP support of MATLAB function
// FREQUENCYIDENTIFICATION- Identify frequencies with largest coefficients.
// By comparing coefficients with large magnitudes which appear in the 
// constructed measurement matrix multiplied by the Fourier transform of the 
// signal, the frequencies which correspond to these largest or "most energetic" 
// frequencies are repeatedly identified. Only the ones that are identified 
// enough to be useful in a coefficient estimation step are kept.
//
// MATLAB code by Craig Gross, Mar 2020
// C implementation by Toni Volkmer, May 2020

#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

#if __STDC_VERSION__ >= 199901L
/* C99 code */
#define C99
  #define STATIC static
  #define INLINE inline
#else
  #define STATIC
  #define INLINE
#endif

#include <complex.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "pocketfft.h"

// function [ inverse ] = modularInverse(p, q)
// %MODULARINVERSE- Calculates p^(-1) mod q for relatively prime integers p and q.
// %Algorithm from https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm#Modular_integers.
// %
// % Syntax:  [ inverse ] = modularInverse(p, q)
// %
// % Inputs:
// %    p: Integer.
// %    q: Integer relatively prime to p.
// %
// % Outputs:
// %    inverse: p^(-1) mod q
// %
// % Author: Craig Gross
// % Mar 2020
// inverse = 0; newInverse = 1;
// r = q; newr = p;
// while newr ~= 0
//   quotient = floor(r / newr);
//   temp = inverse;
//   inverse = newInverse; newInverse = temp - quotient * newInverse;
//   temp = r;
//   r = newr; newr = temp - quotient * newr;
// end
// if r > 1
//   error([num2str(p), ' is not invertible modulo ', num2str(q), '.']);
// end
// if inverse < 0
//   inverse = inverse + q;
// end
// end
STATIC long long modularInverse(long long p, long long q)
// implementation requires p,q >= 0
{
  long long inverse = 0, newInverse = 1, r = q, newr = p;

  while (newr != 0)
  {
	long long quotient = r / newr;
    long long temp = inverse;
	inverse = newInverse; newInverse = temp - quotient * newInverse;
	temp = r;
	r = newr; newr = temp - quotient * newr;
  }

  if (r > 1)
    mexErrMsgIdAndTxt("onePrimeMex:modularInverse",
                      "not invertible");
  if (inverse < 0)
	inverse += q;

  return inverse;
}

// function [ omega ] = reconstruct(r1, r2, s1, s2)
// %RECONSTRUCT- Perform the modular reconstruction for only two remainders.
// %The algorithm is justified by the following calculation:
// % omega = r1 mod s1 = r2 mod s2
// %	implies omega = r1 + s1 * j = r2 mod s2
// %	implies j = s1^-1 (r2 - r1) mod s2
// % Note that 0 <= r1 < s1 and 0 <= j < s2 
// %	implies 0 <= omega = r1 + s1 * j < s1 * s2.
// %
// % Syntax:  function [ omega ] = reconstruct(r1, r2, s1, s2)
// %
// % Inputs:
// %    r1: The remainder of omega mod s1. Can be column vector.
// %    r2: The remainder of omega mod s2. Can be column vector of same length 
// %		as r1.
// %    s1: A positive integer.
// %    s2: A positive integer relatively prime to s1.
// %
// % Outputs:
// %    omega: The unique 0<=omega<s1*s2 satisfying omega = r1 mod s1 = r2 mod s2.
// %		If the remainders are column vector, so is omega, with each entry 
// %		having the corresponding remainders in r1 and r2.
// % Author: Craig Gross
// % Mar 2020
// omega = r1 + s1 .* mod(SFTPlan.modularInverse(s1, s2) .* (r2 - r1), s2);
// end
STATIC void reconstruct(long long *omega, long long len_omega, long long *r2, long long s1, long long s2)
// omega and r2 are arrays of length at least len_omega
{
  long long inv = modularInverse(s1, s2);

  for (long long j = 0; j < len_omega; j++)
  {
    long long mod_val = (inv * (r2[j] - omega[j])) % s2;
    // MATLAB mod guarantees result to be >= 0 but C modulo does not
    if (mod_val < 0)
      mod_val += s2;
    omega[j] = omega[j] + s1 * mod_val;
  }
}
  
// function [ omega ] = modularReconstruction(r, s)
// %MODULARRECONSTRUCTION- Calculate omega using only remainders r = omega mod s.
// %We are guaranteed a unique 0 <= omega < prod(s) by the Chinese remainder 
// %theorem. Note that we assume that the integers in s are relatively prime.
// %
// % Syntax:  [ omega ] = modularReconstruction(r, s)
// %
// % Inputs:
// %    r: Row vector of remainders r = omega mod s satisfying 0 <= r < s.
// %		Optionally, r can also be a matrix, where each column r(:, i) 
// %		represents the remainders of different omega mod s(i), and the
// %		function will simply be applied to all elements.
// %    s: Vector of relatively prime positive integers.
// %
// % Outputs:
// %    omega: The unique 0 <= omega < prod(s) satisfying omega = r mod s.
// %		If the r is a matrix, omega is a column vector with as many rows as r,
// %		with each entry having the corresponding remainders given by the 
// %		columns of r.
// %
// % Other m-files required: modularInverse.m
// % Subfunctions: reconstruct
// %
// % Author: Craig Gross
// % Mar 2020
// % Operate incrementally, starting from first two remainders and working up.
// omega = r(:, 1);
// divisor = s(1);
// for i = 2:length(s)
//   omega = reconstruct(omega, r(:, i), divisor, s(i));
//   divisor = divisor * s(i);
// end
// end
STATIC void modularReconstruction(long long *omega, long long len_omega, long long *r, double *s, long long len_s)
// omega is of length at least len_omega
// r is of length len_omega*len_s
// s is of length len_s
{
  long long divisor = len_omega;
  // Operate incrementally, starting from first two remainders and working up.
  for (long long j = 0; j < len_omega; j++)
    omega[j] = j;

  for (long long i = 0; i < len_s; i++)
  {
	reconstruct(omega, len_omega, r+i*len_omega, divisor, s[i]);
	divisor = divisor * s[i];
  }
}

// function [ product, omega ] = onePrime(plan, j, samples)
// %ONEPRIME- Reconstruct all frequencies from one major prime.
// % Syntax:  [ identified, timesIdentified ] = onePrime(plan, j, samples)
// %     or   [ identified, timesIdentified ] = plan.onePrime(j, samples)
// % Inputs:
// %    j: The index of the prime to use. Should be in 1:plan.K
// %    samples: Samples of the signal at the sampling nodes for the plan.
// % Outputs:
// %    identified: The list of all frequencies identified modulo plan.s(j).
// %    timesIdentified: The number of times each frequency in identified
// %		was identified.
// % See also: GENERALFASTMULTIPLY FREQUENCYIDENTIFICATION
// % Author: Craig Gross
// % Apr 2020
// product = zeros(plan.s(j) * (1 + plan.tSum{j}(end)), 1);
// A = samples(1:plan.s(j));
// product(1:plan.s(j)) = (1 / plan.s(j)) * fft(A);
// start = plan.s(j);
// for i = 1:length(plan.t{j})
//   fftSize = plan.s(j) * plan.t{j}(i);
//   A = samples(start + 1: start + fftSize);
//   product(start + 1: start + fftSize) =...
// 		(1 / fftSize) * fft(A);
//   % Move into next part of product
//   start = start + plan.s(j) * plan.t{j}(i);
// end
// 
// h = (0:plan.s(j) - 1)'; % All residues of s(j)
// r = h + 1; % Their locations in product
// divisor = plan.s(j) * prod(plan.t{j}); 
// remainders = zeros(plan.s(j), length(plan.t{j}));
// rbarBase = plan.s(j);
// for i = 1:length(plan.t{j})
//   % All pairs of residues of s(j) and residues of t{j}(i).
//   rbar = rbarBase + plan.s(j) * (0:(plan.t{j}(i) - 1)) + h + 1;
//   % Find the residues of t{j}(i) which match the original element best
//   [~, bmin] = min( abs(product(r) - product(rbar)), [], 2 );
//   remainders(:, i) = mod(plan.s(j) * (bmin - 1) + h, plan.t{j}(i));
// 
//   % Move into the part of product corresponding to t{j}(i + 1)
//   % or s(j + 1) if there are no more t{j} values.
//   rbarBase = rbarBase + plan.s(j) * plan.t{j}(i);
// end
// omega = SFTPlan.modularReconstruction([h, remainders], ...
// 	[plan.s(j), plan.t{j}]);
// 
// % Ensure frequencies are in the canonical interval
// % -ceil(divisor / 2) + 1:floor(divisor / 2)
// % and are weighted the proper number of times.
// omega = repmat(mod(omega - 1 + ceil(divisor / 2), divisor) ...
// 	+ 1 - ceil(divisor / 2), plan.weights(j), 1);
// 
// end
STATIC void onePrime(long long *omega, double _Complex *product, double _Complex *samples_r, double _Complex *samples_rbar, long long s_j, double *t_j, long long len_t_j)
{
  long long i, k;
  cfft_plan p;
  for (k = 0; k < s_j; k++)
    product[k] = samples_r[k];
  p = make_cfft_plan(s_j);
  cfft_forward(p, (double *) product, 1.0/s_j);
  destroy_cfft_plan(p);

  long long start = 0;
  for (i = 0; i < len_t_j; i++)
  {
    long long t_j_i = llrint(t_j[i]);
    double _Complex *A = product + start + s_j;
	long long fftSize = s_j * t_j_i;
    for (k = 0; k < fftSize; k++)
      A[k] = samples_rbar[start+k];
    p = make_cfft_plan(fftSize);
    cfft_forward(p, (double *) A, 1.0/fftSize);
    destroy_cfft_plan(p);
    
	//Move into next part of product
	start +=  s_j * t_j_i;
  }

  long long divisor = s_j;
  for (i = 0; i < len_t_j; i++)
    divisor *= llrint(t_j[i]);
  long long *remainders = (long long *) malloc(s_j * len_t_j * sizeof(long long));
  long long rbarBase = s_j;
  for (i = 0; i < len_t_j; i++)
  {
    long long t_j_i = llrint(t_j[i]);
    long long bmin_minus1[s_j];
    double val_bmin_minus1[s_j];
    for (long long r = 0; r < s_j; r++)
    {
      long long rbar_0 = rbarBase + s_j * 0 + r;
      bmin_minus1[r] = 0;
      val_bmin_minus1[r] = cabs(product[r] - product[rbar_0]);
    }
    for (long long k = 1; k < t_j_i; k++)
    {
      long long rbar_pre = rbarBase + s_j * k;
      for (long long r = 0; r < s_j; r++)
      {
        long long rbar = rbar_pre + r;
        double val_cur = cabs(product[r] - product[rbar]);
        if (val_cur < val_bmin_minus1[r])
        {
          bmin_minus1[r] = k;
          val_bmin_minus1[r] = val_cur;
        }
      }
    }
    for (long long r = 0; r < s_j; r++)
      remainders[r  + i*s_j] = (s_j * bmin_minus1[r] + r) % t_j_i;
    
	rbarBase += s_j * t_j_i;
  }

  modularReconstruction(omega, s_j, remainders, t_j, len_t_j);
  
  for (long long r = 0; r < s_j; r++)
  {
    omega[r] = omega[r] % divisor;
    if (omega[r] < -(divisor/2))
      omega[r] += divisor;
    else if (omega[r] > divisor/2)
      omega[r] -= divisor;
  }
  
  free(remainders);
}

// function [ identified ] = frequencyIdentification(plan, product)
// %FREQUENCYIDENTIFICATION- Identify frequencies with largest coefficients.
// %By comparing coefficients with large magnitudes which appear in the 
// %constructed measurement matrix multiplied by the Fourier transform of the 
// %signal, the frequencies which correspond to these largest or "most energetic" 
// %frequencies are repeatedly identified. Only the ones that are identified 
// %enough to be useful in a coefficient estimation step are kept.
// % Syntax:  [ identified ] = frequencyIdentification(plan, product)
// %      or  [ identified ] = plan.frequencyIdentification(product)
// % Inputs:
// %    product: Extended row tensor product measurement matrix multiplied by 
// %		various Fourier transforms of given signal. Should have form calculated 
// %		by GENERALFASTMULTIPLY.
// % Outputs:
// %    identified: Vector of reconstructed frequencies. Only frequencies which 
// %		have been reconstructed more half total number of primes times will be 
// %		returned.
// % See also: GENERALFASTMULTIPLY
// % Author: Craig Gross
// % Mar 2020
// L = plan.sSum(end);
// 
// identified = zeros(1, L);
// timesIdentified = zeros(1, L);
// numIdentified = 0;
// 
// % Below, we consider all residues h of each major prime s(j). If a 
// % significant frequency = h mod s(j), we calculate its remainders modulo each 
// % t{j}(i) by searching over these residues for the entry in the product 
// % closest to the original entry corresponding to h mod s(j). The original 
// % entry is indexed with r and the entries in the product corresponding to 
// % residues of t{j}(i) are indexed with rbar. 
// %
// % See GENERALFASTMULTIPLY for explanation of indexing scheme.
// rbarBase = L;
// for j = 1:plan.K
//   h = (0:plan.s(j) - 1)'; % All residues of s(j)
//   r = plan.sSum(j) + h + 1; % Their locations in product
//   divisor = plan.s(j) * prod(plan.t{j}); 
//   remainders = zeros(plan.s(j), length(plan.t{j}));
//   for i = 1:length(plan.t{j})
//     % All pairs of residues of s(j) and residues of t{j}(i).
//     rbar = rbarBase + plan.s(j) * (0:(plan.t{j}(i) - 1)) + h + 1;
//     % Find the residues of t{j}(i) which match the original element best
//     [~, bmin] = min( abs(product(r) - product(rbar)), [], 2 );
//     remainders(:, i) = mod(plan.s(j) .* (bmin - 1) + h, plan.t{j}(i));
// 
//     % Move into the part of product corresponding to t{j}(i + 1)
//     % or s(j + 1) if there are no more t{j} values.
//     rbarBase = rbarBase + plan.s(j) * plan.t{j}(i);
//   end
//   omega = SFTPlan.modularReconstruction([h, remainders], ...
// 		[plan.s(j), plan.t{j}]);
// 
//   % Ensure frequencies are in the canonical interval
//   % -ceil(divisor / 2) + 1:floor(divisor / 2)
//   omega = mod(omega - 1 + ceil(divisor / 2), divisor) ...
// 		+ 1 - ceil(divisor / 2);
// 
//   % Track how many times each frequency has been found
//   [~, omegaMatch, idMatch] = intersect(omega, identified(1:numIdentified));
//   % Frequencies already found
//   % For repeated primes, we count this identifications weights(j) times
//   timesIdentified(idMatch) = timesIdentified(idMatch) + plan.weights(j);
//   % Frequencies to add to list
//   omegaNotMatch = setdiff(1:plan.s(j), omegaMatch);
//   identified(numIdentified+1:numIdentified+length(omegaNotMatch)) =...
// 		omega(omegaNotMatch);
//   % For repeated primes, we count this identifications weights(j) times
//   timesIdentified(numIdentified+1:numIdentified+length(omegaNotMatch))=...
//     plan.weights(j);
//   numIdentified = numIdentified + length(omegaNotMatch);
// end
// 
// % Need to be identified more than the TOTAL number of primes including repeats
// identified = identified(timesIdentified > sum(plan.weights) / 2);
// end
STATIC void frequencyIdentificationMex(double _Complex *product_j, double *omega_j, long long len_s, long long j, double _Complex *samples, double s_j, double sSum_j, double sSum_end, double *t_j, long long len_t_j, double *s_j_times_tSum_j_end_array)
{
  long long r_start = llrint(sSum_j);
  long long rbar_start = llrint(sSum_end);

  for (long long jj = 0; jj < j; jj++)
    rbar_start += llrint(s_j_times_tSum_j_end_array[jj]);

  long long s_j_int = llrint(s_j);

  long long omega_j_int_array[s_j_int];

  onePrime(omega_j_int_array, product_j, samples+r_start, samples+rbar_start, s_j_int, t_j, len_t_j);

  long long divisor = s_j_int;
  for (long long k = 0; k < len_t_j; k++)
    divisor *= llrint(t_j[k]);

  long long ceil_divisor_half = (divisor % 2 != 0) ? divisor / 2 + 1 : divisor / 2;

  for (long long k = 0; k < s_j_int; k++)
  {
    long long tmp = omega_j_int_array[k] - 1 + ceil_divisor_half % divisor;
    if (tmp < 0) tmp += divisor;
    omega_j[k] = (double) (tmp + 1 - ceil_divisor_half);
  }
}

void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
      
  if (nrhs < 5)
    mexErrMsgIdAndTxt("parFreqIdentMex:nrhs",
                      "Five inputs required. Usage: [product, omega] = parFreqIdentMex(samples, plan.s, plan.sSum, plan.t, s_j_times_tSum_j_end_array)");

  if (nlhs != 2)
    mexErrMsgIdAndTxt("parFreqIdentMex:nlhs",
                      "Two outputs required. Usage: [product, omega] = parFreqIdentMex(samples, plan.s, plan.sSum, plan.t, s_j_times_tSum_j_end_array)");

  if (!mxIsComplex(prhs[0]) || mxGetClassID(prhs[0])!=mxDOUBLE_CLASS)
    mexErrMsgIdAndTxt("parFreqIdentMex:samplesDataType",
                      "First input 'samples' must be complex double array");
    
  long long num_samples = mxGetNumberOfElements(prhs[0]);
  double _Complex *samples = (double _Complex*) mxGetComplexDoubles(prhs[0]);
  
  if (mxIsComplex(prhs[1]) || mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
    mexErrMsgIdAndTxt("parFreqIdentMex:sDataType",
                      "Second input 'plan.s' must be real double array");
  long long len_s = mxGetNumberOfElements(prhs[1]);
  double *s_array = (double *) mxGetDoubles(prhs[1]);

  if (mxIsComplex(prhs[2]) || mxGetClassID(prhs[2])!=mxDOUBLE_CLASS
          || mxGetNumberOfElements(prhs[2])!=len_s+1)
    mexErrMsgIdAndTxt("parFreqIdentMex:sSumDataTypeOrLength",
                      "Third input 'plan.sSum' must be real double array of length: length(plan.s)+1");
  double *sSum_array = (double *) mxGetDoubles(prhs[2]);

  if (!mxIsCell(prhs[3]) || mxGetNumberOfElements(prhs[3]) != len_s)
    mexErrMsgIdAndTxt("parFreqIdentMex:tDataTypeorLength",
                      "Forth input 'plan.t' must be cell array of length: length(plan.s)");
  double **t_array = (double **) malloc(len_s * sizeof(double *));
  long long *t_lengths = (long long *) malloc(len_s * sizeof(long long *));
  for (long long j = 0; j < len_s; j++)
  {
    const mxArray *t_j = mxGetCell(prhs[3], j);
    t_lengths[j] = mxGetNumberOfElements(t_j);
    if (mxIsComplex(t_j) || mxGetClassID(t_j)!=mxDOUBLE_CLASS)
      mexErrMsgIdAndTxt("parFreqIdentMex:tContentDataTypeOrLength",
                        "Entries of forth input 'plan.t' must be real double array");
    t_array[j] = (double *) mxGetDoubles(t_j);
  }

  if (mxIsComplex(prhs[4]) || mxGetClassID(prhs[4])!=mxDOUBLE_CLASS
          || mxGetNumberOfElements(prhs[4])!=len_s)
    mexErrMsgIdAndTxt("parFreqIdentMex:arg5",
                      "Fifth input must be real double array of length: length(plan.s)");
  double *s_j_times_tSum_j_end_array = (double *) mxGetDoubles(prhs[4]);

#ifdef _OPENMP
  if (nrhs >= 6)
  {
    if (mxGetNumberOfElements(prhs[5]) != 1 || lrint(mxGetScalar(prhs[5])) < 1)
      mexErrMsgIdAndTxt("parFreqIdentMex:nthreads",
                        "Sixth argument has to be >= 1. Usage: [ product ] = parFreqIdentMex(samples, stSum, nthreads)");
    nthreads = lrint(mxGetScalar(prhs[5]));
  }
#endif

  mwSize out_dims[2] = {1, len_s};
  double _Complex **product_data = (double _Complex**) malloc(len_s * sizeof(double _Complex*));
  plhs[0] = mxCreateCellArray(2, out_dims);
  for (long long j = 0; j < len_s; j++)
  {
    long long num_samples_j = llrint(s_j_times_tSum_j_end_array[j]) + llrint(s_array[j]);

    mxArray *product_j_matrix = mxCreateDoubleMatrix(num_samples_j,1,mxCOMPLEX);
    mxSetCell(plhs[0], j, product_j_matrix);
    product_data[j] = (double _Complex*) mxGetComplexDoubles(product_j_matrix);
  }

  double **omega_data = (double**) malloc(len_s * sizeof(double*));
  plhs[1] = mxCreateCellArray(2, out_dims);

  for (long long j = 0; j < len_s; j++)
  {
    mxArray *omega_j_matrix = mxCreateDoubleMatrix(llrint(s_array[j]),1,mxREAL);
    mxSetCell(plhs[1], j, omega_j_matrix);
    omega_data[j] = (double*) mxGetDoubles(omega_j_matrix);
  }

//   printf("nthreads = %d\n", nthreads);
  
  #pragma omp parallel for num_threads(nthreads) schedule(dynamic,4)
  for (long long j = 0; j < len_s; j++)
    frequencyIdentificationMex(product_data[j], omega_data[j], len_s, j, samples, s_array[j], sSum_array[j], sSum_array[len_s], t_array[j], t_lengths[j], s_j_times_tSum_j_end_array);

  free(omega_data);
  free(product_data);
  free(t_lengths);
  free(t_array);
}
