// Run to compile code:
// mex CC='gcc -fopenmp' COPTIMFLAGS='-O3 -march=native -ffast-math' LDOPTIMFLAGS='' -R2018a -outdir @SFTPlan coefficientsForAllFreqMex.c

// Description:
// C MEX implementation with OpenMP support of MATLAB function
// function [ x ] = coefficientForOneFreq(plan, omega, product)
//
// MATLAB code by Craig Gross, Mar 2020
// C implementation by Toni Volkmer, May 2020
// [ x ] = coefficientsForAllFreqMex(product, identified, s, t, tSum, weights, nthreads)

#include "mex.h"
#include "matrix.h"
#include <stdlib.h>
#include <math.h>

#if __STDC_VERSION__ >= 199901L
/* C99 code */
#define C99
  #define STATIC static
  #define INLINE inline
#else
  #define STATIC
  #define INLINE
#endif

#include <complex.h>

#ifdef _OPENMP
#include <omp.h>
#endif

STATIC int comp_real_part(const void * e1, const void * e2) 
{
  double _Complex v1 = *((double _Complex *) e1);
  double _Complex v2 = *((double _Complex *) e2);

  if (creal(v1) < creal(v2))
    return -1;
  else if (creal(v1) > creal(v2))
    return 1;
  return 0;
}

STATIC int comp_imag_part(const void * e1, const void * e2) 
{
  double _Complex v1 = *((double _Complex *) e1);
  double _Complex v2 = *((double _Complex *) e2);

  if (cimag(v1) < cimag(v2))
    return -1;
  else if (cimag(v1) > cimag(v2))
    return 1;
  return 0;
}

// function [ x ] = coefficientForOneFreq(plan, omega, product)
// indices = cell(1, plan.K);
// for i = 1:plan.K
// 	% Standard measurement matrix index
// 	r = mod(omega, plan.s(i)) + 1;
// 	% Tensor product indices
// 	rbar = plan.s(i) * (1 +  plan.tSum{i}(1:end - 1)) + ...
// 		mod(omega, plan.s(i) * plan.t{i}) + 1;
// 	% Each prime should be counted weights(i) number of times
// 	indices{i} = repmat([r, rbar], 1, plan.weights(i));
// end
// 
// coeffCell = cell(plan.K, 1);
// for i = 1:plan.K
// 	coeffCell{i} = product{i}(indices{i});
// end
// coeff = cell2mat(coeffCell);
// 
// x = median(real(coeff)) + 1i .* median(imag(coeff));
// end
STATIC double _Complex coefficientForOneFreq(double _Complex **product_array,
        long long len_s, long long *product_lengths, double identified_array_k,
        double *s_array, double **t_array, long long *t_lengths,
        double **tSum_array, double *weights_array)
{
  double result_r, result_i;
  long long coefficients_length = 0;
  for (long long i = 0; i < len_s; i++)
    coefficients_length += (llrint(t_lengths[i])+1) * llrint(weights_array[i]);

  double _Complex * coefficients = (double _Complex *) malloc(coefficients_length * sizeof(double _Complex));

  long long coefficients_counter = 0;
  for (long long i = 0; i < len_s; i++)
  {
    long long weight = llrint(weights_array[i]);
    long long omega = llrint(identified_array_k);
    long long s_i = llrint(s_array[i]);
    long long r = omega % s_i;
    if (r < 0) r+= s_i;
    for (long long iw = 0; iw < weight; iw++)
      coefficients[coefficients_counter++] = product_array[i][r];

    long long t_lengths_i = llrint(t_lengths[i]);
    for (long long m = 0; m < t_lengths_i; m++)
    {
      long long d2 = s_i * llrint(t_array[i][m]);
      long tmp = omega % d2;
      if (tmp < 0) tmp += d2;
      long long rbar_m = s_i * (1 +  llrint(tSum_array[i][m])) + tmp;

//       if (rbar_m >= product_lengths[i])
//         mexErrMsgIdAndTxt("coefficientsForAllFreqMex:rbarm",
//                           "Invalid rbar_m value");
        
      for (long long iw = 0; iw < weight; iw++)
        coefficients[coefficients_counter++] = product_array[i][rbar_m];
    }
  }

//   if (coefficients_length != coefficients_counter)
//     mexErrMsgIdAndTxt("coefficientsForAllFreqMex:invalidCounter",
//                       "Invalid coefficients counter value");

  qsort(coefficients, coefficients_length, sizeof(double _Complex), comp_real_part);
  if (coefficients_length % 2 == 1)
    result_r = creal(coefficients[coefficients_length/2]);
  else
    result_r = 0.5 * (creal(coefficients[coefficients_length/2-1]) + creal(coefficients[coefficients_length/2]));

  qsort(coefficients, coefficients_length, sizeof(double _Complex), comp_imag_part);
  if (coefficients_length % 2 == 1)
    result_i = cimag(coefficients[coefficients_length/2]);
  else
    result_i = 0.5 * (cimag(coefficients[coefficients_length/2-1]) + cimag(coefficients[coefficients_length/2]));

  free(coefficients);

  return result_r + I * result_i;
}

// Coefficient estimate each frequency
// x = zeros(1, length(identified));
// for j = 1:length(identified)
// 	x(j) = plan.coefficientForOneFreq(identified(j), product);
// end
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
#ifdef _OPENMP
  int nthreads = omp_get_max_threads();
#else
  int nthreads = 1;
#endif
      
  if (nrhs < 2)
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:nrhs",
                      "Two inputs required. Usage: [ x ] = coefficientsForAllFreqMex(product, identified, s, t, tSum, weights)");

  if (nlhs != 1)
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:nlhs",
                      "One output required. Usage: [ x ] = coefficientsForAllFreqMex(product, identified, s, t, tSum, weights)");

  if (!mxIsCell(prhs[0]))
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:productDataType",
                      "First input 'product' must be cell array");
  
  if (mxGetM(prhs[0]) != 1)
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:nColsNotOne",
                      "First input 'product' must be cell array with one rows");

  long long len_product = mxGetN(prhs[0]);

  if (mxIsComplex(prhs[2]) || mxGetClassID(prhs[2])!=mxDOUBLE_CLASS)
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:sDataType",
                      "Third input 'plan.s' must be real double array");
  long long len_s = mxGetNumberOfElements(prhs[2]);
  double *s_array = (double *) mxGetDoubles(prhs[2]);
  
  if (len_s != len_product)
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:sDataType",
                      "Third input 'plan.s' must have same length as first input 'product'");

  double _Complex **product_array = (double _Complex **) malloc(len_product * sizeof(double _Complex *));
  long long *product_lengths = (long long *) malloc(len_product * sizeof(long long *));
  for (long long j = 0; j < len_product; j++)
  {
    const mxArray *product_j = mxGetCell(prhs[0], j);
    product_lengths[j] = mxGetNumberOfElements(product_j);
    if (!mxIsComplex(product_j) || mxGetClassID(product_j)!=mxDOUBLE_CLASS)
    {
      free(product_array);
      free(product_lengths);
      mexErrMsgIdAndTxt("coefficientsForAllFreqMex:productDataType",
                        "Entries of first input 'product' must be complex double array");
      return;
    }
    product_array[j] = (double _Complex *) mxGetComplexDoubles(product_j);
  }

  if (mxIsComplex(prhs[1]) || mxGetClassID(prhs[1])!=mxDOUBLE_CLASS)
  {
    free(product_array);
    free(product_lengths);
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:identifiedDataType",
                      "Second input 'identified' must be real double array");
    return;
  }
  long long len_identified = mxGetNumberOfElements(prhs[1]);
  double *identified_array = (double *) mxGetDoubles(prhs[1]);

  if (!mxIsCell(prhs[3]) || mxGetNumberOfElements(prhs[3]) != len_s)
  {
    free(product_array);
    free(product_lengths);
    mexErrMsgIdAndTxt("parFreqIdentMex:tDataTypeorLength",
                      "Forth input 'plan.t' must be cell array of length: length(plan.s)");
    return;
  }
  double **t_array = (double **) malloc(len_s * sizeof(double *));
  long long *t_lengths = (long long *) malloc(len_s * sizeof(long long *));
  for (long long j = 0; j < len_s; j++)
  {
    const mxArray *t_j = mxGetCell(prhs[3], j);
    t_lengths[j] = mxGetNumberOfElements(t_j);
    if (mxIsComplex(t_j) || mxGetClassID(t_j)!=mxDOUBLE_CLASS)
    {
      free(product_array);
      free(product_lengths);
      free(t_array);
      free(t_lengths);
      mexErrMsgIdAndTxt("parFreqIdentMex:tContentDataTypeOrLength",
                        "Entries of forth input 'plan.t' must be real double array");
      return;
    }
    t_array[j] = (double *) mxGetDoubles(t_j);
  }

  if (!mxIsCell(prhs[4]) || mxGetNumberOfElements(prhs[4]) != len_s)
  {
    free(product_array);
    free(product_lengths);
    free(t_array);
    free(t_lengths);
    mexErrMsgIdAndTxt("parFreqIdentMex:tSumDataTypeorLength",
                      "Fifth input 'plan.tSum' must be cell array of length: length(plan.s)");
    return;
  }
  double **tSum_array = (double **) malloc(len_s * sizeof(double *));
  for (long long j = 0; j < len_s; j++)
  {
    const mxArray *tSum_j = mxGetCell(prhs[4], j);

    if (mxIsComplex(tSum_j) || mxGetClassID(tSum_j)!=mxDOUBLE_CLASS
            || mxGetNumberOfElements(tSum_j) != t_lengths[j]+1)
    {
      free(product_array);
      free(product_lengths);
      free(t_array);
      free(t_lengths);
      mexErrMsgIdAndTxt("parFreqIdentMex:tSumContentDataTypeOrLength",
                        "Entries of fifth input 'plan.tSum' must be real double array of correct length");
      return;
    }
    tSum_array[j] = (double *) mxGetDoubles(tSum_j);
  }

  if (mxIsComplex(prhs[5]) || mxGetClassID(prhs[5])!=mxDOUBLE_CLASS)
  {
    free(product_array);
    free(product_lengths);
    free(t_array);
    free(t_lengths);
    mexErrMsgIdAndTxt("coefficientsForAllFreqMex:weightsDataTypeOrLength",
                      "Sixth input 'plan.weights' must be real double array of length: length(plan.s)");
    return;
  }
  mxGetNumberOfElements(prhs[5]);
  double *weights_array = (double *) mxGetDoubles(prhs[5]);
  
#ifdef _OPENMP
  if (nrhs >= 7)
  {
    if (mxGetNumberOfElements(prhs[6]) != 1 || lrint(mxGetScalar(prhs[6])) < 1)
    {
      free(product_array);
      free(product_lengths);
      free(t_array);
      free(t_lengths);
      mexErrMsgIdAndTxt("coefficientsForAllFreqMex:nthreads",
                        "Seventh argument has to be >= 1. Usage: [ x ] = coefficientsForAllFreqMex(product, identified, s, t, tSum, weights, nthreads)");
      return;
    }
    nthreads = lrint(mxGetScalar(prhs[6]));
  }
#endif

  plhs[0] = mxCreateDoubleMatrix(1,len_identified,mxCOMPLEX);
  double _Complex *allcoefficients = (double _Complex*) mxGetComplexDoubles(plhs[0]);

//   printf("nthreads = %d\n", nthreads);
  
  #pragma omp parallel for num_threads(nthreads) schedule(dynamic,4)
  for (long long k = 0; k < len_identified; k++)
    allcoefficients[k] = coefficientForOneFreq(product_array, len_s, product_lengths, identified_array[k], s_array, t_array, t_lengths, tSum_array, weights_array);

  free(tSum_array);
  free(t_lengths);
  free(t_array);
  free(product_lengths);
  free(product_array);
}
