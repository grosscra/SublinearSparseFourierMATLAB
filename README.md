# Sublinear-Time Sparse Fourier Transform (for MATLAB)

This project is a MATLAB based implementation of the sparse Fourier transform algorithm (Algorithm 3) from [Iwen, 2013]. 

Given sampling access to a signal, a sparse approximation of its discrete Fourier transform will be calculated. The only parameters necessary are the bandwidth of the signal, as well as the desired sparsity level. Optional parameters are available for faster execution at the cost of potentially less accurate results.

## Usage

### `SFTPlan` object

After downloading the repository, the main algorithm is implemented using the `SFTPlan` object. Navigate to the downloaded repository or add it to your path to have access to the `SFTPlan` object.

Type `help SFTPlan` into the MATLAB Command Window for a description, a list of properties, and public methods (the only one being ``SFTPlan.execute``). Additionally, typing `help SFTPlan.property` or `help SFTPlan.method` (replacing `property` or `method` with the property or method name) will return documentation for a desired property or method.

The general work flow is to create a plan using the constructor `plan = SFTPlan(N, k, executionMode, ...)` where `N` is the bandwidth, `k` is the sparsity, and `executionMode` is a string for either deterministic or random execution. Then access the sampling nodes for this plan using the property `plan.nodes` to produce samples of the signal at these points. Finally, run `[coefficients, frequencies] = plan.execute(samples)` to obtain the sparse Fourier approximation.

The documentation for the class, the constructor, and `execute` are good places to start. Default parameters are chosen to satisfy theoretical accuracy guarantees, but can usually be decreased a good amount for better runtime without sacrificing much accuracy. Experiment at your own risk!

### Wrapper functions

Alternatively, rather than reusing a plan using the `execute` method multiple times, there are two wrapper functions which perform a one-off creation/execution of deterministic and random plans respectively.

After downloading the repository, the deterministic wrapper is implemented in `Functions/fourierApproximate.m`. After navigating to the `Functions` directory or adding it to your path, type `help fourierApproximate` in the MATLAB Command Window for syntax, usage instructions, and other tips. For the random version which generally runs more quickly at the cost of error guarantees holding only with high probability, use `Functions/randomFourierApproximate.m`.

### Parallel implementations

Two parallel implementations of the `execute` method are included as drop-in replacements: `parExecute` and `mexExecute`. Both use the number of threads specified in MATLAB by `maxNumCompThreads`.

The `parExecute` method restructures the algorithm to take advantage of MATLAB's Parallel Computing Toolbox's `parfor` statements. However, this implementation is highly unoptimized an is generally slower than the serial implementation.

The `mexExecute` method implements the restructured algorithm from `parExecute` in C, making use of [PocketFFT](https://gitlab.mpcdf.mpg.de/mtr/pocketfft), a copy of which is included in this repository. This version is parallelized using OpenMP. To build the necessary mex files, run `make` in the top level of this repository (**note**: it is necessary to build with a version of gcc < 9.3). This implementation is still slightly buggy, but in general runs much faster than the `execute` and `parExecute` versions.

### Tests

For examples of the above classes and wrappers, see the `Tests` directory. If a test requiring `SFTPlan` is run from the `Tests` directory, the parent directory is added to the path; if a test requiring the wrapper functions are run from the `Tests` directory, the `Functions` directory is added to the path.

## See also

This code is a reimplementation of only the nonequispaced sparse Fourier transform used in DMSFT in the [MSU Sparse Fourier Repository](https://sourceforge.net/projects/aafftannarborfa/), originally implemented by Ruochuan Zhang. For a reimplementation of the discrete layer on top, see grosscra/SFTDiscretizerMATLAB> which makes use of this repository for testing.

## References

* [Mark Iwen](https://users.math.msu.edu/users/iwenmark/index.html), **Improved approximation guarantees for sublinear-time Fourier algorithms**, Applied and Computational Harmonic Analysis, 34(1)(2013), pp. 57-82, [https://www.sciencedirect.com/science/article/pii/S1063520312000462](https://www.sciencedirect.com/science/article/pii/S1063520312000462). ([Preprint](https://users.math.msu.edu/users/iwenmark/Papers/Iwen_Improved.pdf) available on the author's webpage.)
