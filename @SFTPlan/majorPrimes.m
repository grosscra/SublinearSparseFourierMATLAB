function [ s ] = majorPrimes(plan)
%MAJORPRIMES- Constructs major primes for measurement matrix.
%The major primes are calulated so that the first major prime s(1) has a 
%sequence of powers of primes smaller than s(1) such that their product is at 
%least N / s(1). The remaining major primes are those sequentially following 
%the above calculated s(1). We determine 
%    K = floor(C * k * floor(log(N) / log(s(1)))) + 1 
%total major primes as directed in [Iwen, 2013].
%
% Syntax:  [ s ] = majorPrimes(plan)
%     or:  [ s ] = plan.majorPrimes()
%
% Inputs:
%    plan: An SFTPlan object.
%
% Outputs:
%    s: Length K row vector of sequential major primes with K as in the 
%		description above.
%
% See also: MINORPRIMES

% Author: Craig Gross
% Mar 2020

% Find the first prime s(1) which has a set of minor "primes" less than it. These 
% minor primes are each a power of a prime such that the total product of these 
% minor primes is at least N / s(1).

numPrimes = length(plan.primeList);
numMinorPrimes = 0;
firstPrimeFound = false;
while ~firstPrimeFound
	numMinorPrimes = numMinorPrimes + 1;

	if numMinorPrimes == numPrimes % This should be rare
		plan.primeList = primes(2 * plan.primeList(end));
		numPrimes = length(plan.primeList);
	end

	s1 = plan.primeList(numMinorPrimes + 1);
	tBase = plan.primeList(1:numMinorPrimes);

	% Ensure that for each minor prime base tBase(i), 
	%	tBase(i)^alpha(i) < s1 < tBase(i)^(alpha(i) + 1)
	alpha = floor(log(s1) ./ log(tBase));
	if prod(tBase .^ alpha) >= plan.N
		firstPrimeFound = true;
	end
end

% Set remaining primes

K = floor(plan.C * plan.k * floor(log(plan.N) / log(s1))) + 1;
while numMinorPrimes + K > length(plan.primeList) % This should be very rare
	plan.primeList = primes(2 * plan.primeList(end)); % Make primeList big enough
end

numMinorPrimes = numMinorPrimes + plan.primeShift;
s = plan.primeList(numMinorPrimes + 1: numMinorPrimes + K);

end
