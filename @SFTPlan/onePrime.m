function [ product, omega ] = onePrime(plan, j, samples)
%ONEPRIME- Reconstruct all frequencies from one major prime.
%
% Syntax:  [ identified, timesIdentified ] = onePrime(plan, j, samples)
%     or   [ identified, timesIdentified ] = plan.onePrime(j, samples)
%
% Inputs:
%    j: The index of the prime to use. Should be in 1:plan.K
%    samples: Samples of the signal at the sampling nodes for the plan.
%
% Outputs:
%	product: The FFTs of all products of plan.s(j) with minor primes.
%	omega: The list of all frequencies identified modulo plan.s(j).
%
% See also: GENERALFASTMULTIPLY FREQUENCYIDENTIFICATION

% Author: Craig Gross
% Apr 2020

product = zeros(plan.s(j) * (1 + plan.tSum{j}(end)), 1);
A = samples(1:plan.s(j));
product(1:plan.s(j)) = (1 / plan.s(j)) * fft(A);
start = plan.s(j);
for i = 1:length(plan.t{j})
	fftSize = plan.s(j) * plan.t{j}(i);
	A = samples(start + 1: start + fftSize);
	product(start + 1: start + fftSize) =...
		(1 / fftSize) * fft(A);
	% Move into next part of product
	start = start + plan.s(j) * plan.t{j}(i);
end

h = (0:plan.s(j) - 1)'; % All residues of s(j)
r = h + 1; % Their locations in product
divisor = plan.s(j) * prod(plan.t{j}); 
remainders = zeros(plan.s(j), length(plan.t{j}));
rbarBase = plan.s(j);
for i = 1:length(plan.t{j})
	% All pairs of residues of s(j) and residues of t{j}(i).
	rbar = rbarBase + plan.s(j) * (0:(plan.t{j}(i) - 1)) + h + 1;
	% Find the residues of t{j}(i) which match the original element best
	[~, bmin] = min( abs(product(r) - product(rbar)), [], 2 );
	remainders(:, i) = mod(plan.s(j) * (bmin - 1) + h, plan.t{j}(i));

	% Move into the part of product corresponding to t{j}(i + 1)
	% or s(j + 1) if there are no more t{j} values.
	rbarBase = rbarBase + plan.s(j) * plan.t{j}(i);
end
omega = SFTPlan.modularReconstruction([h, remainders], ...
	[plan.s(j), plan.t{j}]);

% Ensure frequencies are in the canonical interval
% -ceil(divisor / 2) + 1:floor(divisor / 2)
% and are weighted the proper number of times.
omega = repmat(mod(omega - 1 + ceil(divisor / 2), divisor) ...
	+ 1 - ceil(divisor / 2), plan.weights(j), 1);

end
