function [ coefficients, frequencies ] = execute(plan, samples)
%EXECUTE- Constructs sparse Fourier approximation to a signal
%Takes advantage of the precomputed plan information and the samples that must be 
%evaluated at the nodes in [0, 1] for the given plan to return a 2 * k sparse 
%approximation of the Fourier coefficients of a periodic function in the bandwidth N.
%
% Syntax:  [ coefficients, frequencies ] = execute(plan, samples)
%      or  [ coefficients, frequencies ] = plan.execute(samples)
%
% Inputs:
%    samples: Either a column vector of function evaluations of the signal at the 
%		 sampling nodes in plan.nodes or a function handle. In the latter case, the 
%		 function will sample the function handle at plan.nodes. This function 
%		 handle must accept vectorized input.
%
% Outputs:
%    coefficients: Approximations of the 2k most energetic Fourier coefficients.
%    frequencies: The 2k frequencies corresponding to the output coefficients.
%		The frequencies are given by their canonical values in the interval
%		-ceil(N / 2) + 1:floor(N / 2). To obtain 0-indexed discrete 
%		frequencies with bandwidth N, one should apply mod(frequencies, N).
%
% See also: MEXEXECUTE PAREXECUTE

% Author: Craig Gross
% Mar 2020

if isa(samples, 'function_handle')
	samples = samples(plan.nodes);
end

if length(samples) ~= plan.stSum(end)
	error('Samples must have same length as the sampling nodes in the plan.');
end

product = plan.generalFastMultiply(samples);
identified = plan.frequencyIdentification(product);
[coefficients, frequencies] = plan.coefficientEstimate(product, identified);

end
