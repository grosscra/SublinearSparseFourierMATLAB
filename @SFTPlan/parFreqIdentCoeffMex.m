%PARFREQIDENTCOEFFMEX- Identify energetic frequency and coefficients in parallel.
% C MEX implementation with OpenMP support of MATLAB function
% FREQUENCYIDENTIFICATION (identify frequencies with largest coefficients) and
% COEFFICIENTESTIMATE (estimate values of coefficients).
% By comparing coefficients with large magnitudes which appear in the 
% constructed measurement matrix multiplied by the Fourier transform of the 
% signal, the frequencies which correspond to these largest or "most energetic" 
% frequencies are repeatedly identified. Only the ones that are identified 
% enough to be useful in a coefficient estimation step are kept.
%
% Syntax: [coefficients, frequencies] = parFreqIdentCoeffMex(plan, samples, s, sSum, s_j_times_tSum_j_end_array, tSum, weights, nthreads)
%      or [coefficients, frequencies] = plan.parFreqIdentCoeffMex(samples, s, sSum, s_j_times_tSum_j_end_array, tSum, weights, nthreads)
%
% Inputs:
%    samples: A column vector of function evaluations of the signal at the 
%		 sampling nodes in plan.nodes.
%	s: Row vector of major primes from plan
%	sSum: Allows indexing into different regions of measurement matrix,
%		see SFTPlan for details.
%	s_j_times_tSum_j_end_array:
%	tSum: Allows indexing into different regions of measurement matrix,
%		see SFTPlan for details.
%	weights: A length K row vector containing weight given to each prime in s.
%	nthreads: The number of threads to run with OpenMP.
%
% Outputs:
%    coefficients: Most energetic Fourier coefficients found by algorithm.
%    frequencies: Frequencies corresponding to output coefficients.
%
% See also: MEXEXECUTE

% Author: Toni Volkmer
% June 2020

