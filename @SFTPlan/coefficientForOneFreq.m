function [ x ] = coefficientForOneFreq(plan, omega, product)
%COEFFICIENTFORONEFREQ-Compute the coefficient for one identified frequency.
%Estimates each coefficient separately using prime-by-prime FFT information.
%
% Syntax:  [ x ] = coefficientForOneFreq(plan, omega, product)
%      or  [ x ] = plan.coefficientForOneFreq(omega, product)
%
% Inputs:
%	omega: The frequency to compute a coefficient for.
%	product: A length plan.K cell array the jth entry for j in 1:plan.K is the 
%		matrix of all FFTs of size plan.s(j) multiplied by all minor primes.
%		See ONEPRIME for more detail.
%
% Outputs:
%	x: The coefficient estimate corresponding to the identified frequency omega.
%
% See also: COEFFICIENTESTIMATE ONEPRIME

% Author: Craig Gross
% Apr 2020

indices = cell(1, plan.K);
for i = 1:plan.K
	% Standard measurement matrix index
	r = mod(omega, plan.s(i)) + 1;
	% Tensor product indices
	rbar = plan.s(i) * (1 +  plan.tSum{i}(1:end - 1)) + ...
		mod(omega, plan.s(i) * plan.t{i}) + 1;
	% Each prime should be counted weights(i) number of times
	indices{i} = repmat([r, rbar], 1, plan.weights(i));
end

coeffCell = cell(plan.K, 1);
for i = 1:plan.K
	coeffCell{i} = product{i}(indices{i});
end
coeff = cell2mat(coeffCell);

x = median(real(coeff)) + 1i .* median(imag(coeff));

end
