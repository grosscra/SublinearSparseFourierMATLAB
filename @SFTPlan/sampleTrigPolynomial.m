function [ samples ] = sampleTrigPolynomial(plan, coefficients, frequencies)
%SAMPLETRIGPOLYNOMIAL- Samples a trig poly at the plan's sampling nodes.
%Aliases frequencies and uses inverse fast Fourier transforms.
%
% Syntax:  [ samples ] = sampleTrigPolynomial(plan, coefficients, frequencies)
%      or  [ samples ] = plan.sampleTrigPolynomial(coefficients, frequencies)
%
% Inputs:
%    coefficients: Column vector of s fourier coefficients
%    frequencies: Row vector of corresponding frequencies
%
% Outputs:
%    samples: The samples of the given polynomial at the plan's nodes.
%
% The technique for aliasing was taken from code by Lutz Kaemmerer.
%
% See also: SFTPlan.execute

% Author: Craig Gross (with acknowledgement to Lutz Kaemmerer).
% Apr 2020

samples = zeros(plan.stSum(end), 1);
for j = 1:length(plan.st)
	% Find which product of major/minor primes we work with
	stVal = plan.st(j);
	startIdx = plan.stSum(j);
	% Alias the coefficients
	aliasedCoeffs = accumarray(mod(frequencies.', stVal) + 1, ...
		coefficients, [stVal, 1]);
	% Find our position in the measurement matrix
	samples(startIdx + 1: startIdx + stVal) = stVal .* ifft(aliasedCoeffs);
end

end
