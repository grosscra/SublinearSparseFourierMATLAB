classdef SFTPlan < handle
%SFTPlan- Object used for constructing sparse Fourier approximation of a signal.
%The execution of this plan uses Algorithm 3: Fourier Approximate 2 from [Iwen, 
%2013] to construct a 2 * k sparse approximation of the Fourier coefficients of 
%a periodic function on [0, 1] contained in the bandwidth N. Additionally, the 
%plan precomputes necessary sampling nodes for the desired sparsity and 
%bandwidth (and optional parameters) so that it may be reused for different 
%signals which have been sampled at the necessary points.
%
% SFTPlan Properties:
%	N: Bandwidth
%	k: Sparsity level
%	executionMode: Either 'deterministic' or 'random choice of algorithm
%	primeList: Reference list of primes
%	C: Scaling parameters for number of primes
%	sigma: Probability threshold
%	randomScale: Scales the number of random primes used
%	primeShift: Used to choose larger primes
%	nodes: Sampling nodes requiring signal evaluations
%
% SFTPlan Methods:
%	execute: Run the plan and produce a Fourier approximation to the signal
%	sampleTrigPolynomial: Sample a trig poly at the plan's sampling nodes.
%
% Author: Craig Gross
% Mar 2020
	properties (Access = private)
		% Mandatory inputs

		% N: Bandwidth
		N {mustBeInteger, mustBePositive}
		% k: Sparsity level
		k {mustBeInteger, mustBePositive}
		% executionMode: Char array, either 'deterministic' or 'random' choosing
		% the type of algorithm to execute
		executionMode {mustBeMember(executionMode,...
			{'deterministic', 'random'})} = 'deterministic'

		% Optional inputs for prime choosing.
		
		% primeList: Row vector of sequential primes starting at 2. Allows for 
		% speedup of plan creation if precomputed.
		primeList {mustBeInteger, mustBePositive}
		% C: Scaling parameter for the number of major primes, see creation of 
		% major primes in SFTPlan.majorPrimes or SFTPlan.randomMajorPrimes for 
		% details.
		C {mustBePositive}
		% sigma: Only used for 'random' executionMode. A parameter in (0, 1) 
		% representing the probability with which the resulting algorithm will 
		% produce sufficiently accurate sparse approximation. Its effect on the 
		% number of major primes is detailed in STFPlan.randomMajorPrimes.
		sigma {mustBePositive, mustBeLessThan(sigma, 1)}
		% randomScale: Only used for 'random' executionMode. A parameter which 
		% allows one to change the parameter affecting the number of random primes 
		% used. The number of random primes are chosen proportionally to ~log(N) 
		% (not considering the affect of sigma) with constant of proportionality 
		% randomScale.
		randomScale {mustBePositive}
		% primeShift: After determining the first valid prime to use (denoted 
		% primeList(i)) in majorPrimes, we start at the larger prime 
		% primeList(i + primeShift).
		primeShift {mustBeNonnegative, mustBeInteger}

		% Computed quantities
		
		% s: Row vector of major primes, see SFTPlan.majorPrimes or 
		% SFTPlan.randomMajorPrimes for details.
		s
		% K: Number of major primes.
		K
		% sSum: Allows indexing into different regions of measurement matrix, 
		% see GENERALFASTMULTIPLY for explanation of indexing scheme.
		sSum
		% weights: A length K row vector containing weight given to each prime 
		% in s. This is only used for cases where there can be repeated major 
		% primes, e.g., when a random set is chosen with replacement (see 
		% SFTPlan.randomMajorPrimes for details). It is necessary when 
		% reconstructing frequencies and coefficients in 
		% SFTPlan.frequencyIdentification and SFTPlan.coefficientEstimation.
		weights
		% t: Cell array of minor primes, see SFTPlan.minorPrimes for details.
		t
		% tSum: Allows indexing into different regions of measurement matrix,
		% see GENERALFASTMULTIPLY for explanation of indexing scheme.
		tSum	
		% st: List of products of major and minor primes used for FFTs.
		st
		% stSum: Indices in product array for arranging sampling nodes and FFTs.
		stSum
	end
	properties (SetAccess = private)
		% Column vector of sampling nodes, see SFTPlan.samplingNodes for details
		nodes
	end
	methods
		function plan = SFTPlan(N, k, executionMode, varargin)
			%SFTPlan: Main constructor.
			%Syntax: plan = SFTPlan(N, k, executionMode, ...)
			%	with optional Name, Value pairs for 
			%		C (e.g., SFTPlan(N, k, executionMode 'C', 4))
			%		primeList (e.g., sftPlan(N, k, executionMode,...
			%			'primeList',primeList))
			%		sigma (e.g., SFTPlan(N, k, executionMode 'sigma', 3/4))
			%	or any combination of these Name, Value pairs.
			
			% Handle mandatory arguments
			plan.executionMode = executionMode;
			plan.N = N;
			plan.k = k;

			% Handle optional inputs 
			[plan.primeList, plan.C, plan.sigma, plan.randomScale, plan.primeShift] = ...
				variableInputHandler(varargin, 'primeList', 'C', 'sigma',...
				'randomScale', 'primeShift');
			if isempty(plan.C)
				if strcmpi(executionMode, 'deterministic')
					plan.C = 8;
				else
					plan.C = 14;
				end
			end
			if isempty(plan.primeList)
				plan.primeList = primes(N);
			end
			if isempty(plan.sigma)
				plan.sigma = 2/3;
			end
			if isempty(plan.randomScale)
				plan.randomScale = 21;
			end
			if isempty(plan.primeShift)
				plan.primeShift = 0;
			end

			% Compute remaining arguments
			if strcmpi(executionMode, 'deterministic')
				plan.s = plan.majorPrimes;
				plan.weights = ones(1, length(plan.s));
			else
				sFull = plan.randomMajorPrimes;
				[plan.s, ~, locations] = unique(sFull); % sFull = s(locations)
				% sFull has weights(j) copies of s(j)
				plan.weights = histcounts(locations, 1:length(plan.s) + 1);
			end
			plan.K = length(plan.s);
			plan.sSum = [0, cumsum(plan.s)];
			plan.t = plan.minorPrimes;
			plan.tSum = cellfun(@(x)cumsum([0, x]), plan.t, ...
				'UniformOutput', false);
			stCell = cell(1, plan.K);
			for j = 1:plan.K
				stCell{j} = plan.s(j) .* plan.t{j};
			end
			plan.st = [plan.s, cell2mat(stCell)];
			plan.stSum = cumsum([0, plan.st]);
			plan.nodes = plan.samplingNodes;
		end

		[coefficients, frequencies] = execute(plan, samples)
		[coefficients, frequencies] = parExecute(plan, samples)
		[coefficients, frequencies] = mexExecute(plan, samples)
		[samples] = sampleTrigPolynomial(plan, coefficients, frequencies)
	end
	methods (Access = private)
		[ s ] = majorPrimes(plan)
		[ s ] = randomMajorPrimes(plan)
		[ t ] = minorPrimes(plan)
		[ nodes ] = samplingNodes(plan)
		[ product ] = generalFastMultiply(plan, samples)
		[ identified ] = frequencyIdentification(plan, product)
		[coefficients, frequencies] = coefficientEstimate(plan, product, ...
			identified)
		[ product, omega ] = onePrime(plan, j, samples)
		[ x ] = coefficientForOneFreq(plan, omega, product)
	end
	methods (Static)
		[ omega ] = modularReconstruction(r, s)
		[ inverse ] = modularInverse(p, q)
		[product, omega] = parFreqIdentMex(samples, s, sSum, t, s_j_times_tSum_j_end_array, nthreads);
		[ x ] = coefficientsForAllFreqMex(product, identified, s, t, tSum, weights, nthreads);
		[indentified, x] = parFreqIdentCoeffMex(samples, s, sSum, t, s_j_times_tSum_j_end_array, tSum, weights, nthreads);
	end
end

function [ varargout ] = variableInputHandler(inputs, varargin)
%VARIABLEINPUTHANDLER- Extracts Name, Value inputs from a varargin cell array
%In addition to the inputs cell array of Name, Value pairs, accepts strings 
%corresponding to names to extract from the cell array.
%
% Syntax:  [ varargout ] = variableInputHandler(inputs, ...)
%     with optional trailing string arguments.
%
% Inputs:
%    inputs: A one-dimensional cell array containing strings/character vectors 
%		representing names followed by the value of that property.
%    varargin: Optional strings representing the names of properties to be 
%		extracted from inputs.
%
% Outputs:
%    varargout: For each name given in varargin, will output the 
%		corresponding value as given in inputs. If the name does not exist in 
%		inputs, an empty array will be returned in that position. Checks should 
%		be made for empty arrays in order to assign default values if necessary.

% Author: Craig Gross
% Mar 2020

if nargout ~= length(varargin)
	error('Number of outputs must match number of names input.');
end

names = cell(1, length(inputs) / 2);
for i = 1:2:length(inputs)
	if isstring(inputs{i})
		names{(i + 1) / 2} = char(inputs{i});
	elseif ischar(inputs{i})
		names{(i + 1) / 2} = inputs{i};
	else
		error('Name of input should be character array or string.');
	end
end

for i = 1:length(varargin)
	namesIndex = find(contains(names, varargin{i}));
	if isempty(namesIndex)
		varargout{i} = []; 
	else
		varargout{i} = inputs{2 * namesIndex};
	end
end

end
