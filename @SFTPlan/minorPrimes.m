function [ t ] = minorPrimes(plan)
%MINORPRIMES- Constructs minor "primes" for the given major prime.
%The minor primes are actually powers of primes of the form 
%t(i) = tBase(i) ^ alpha(i), which satisfy prod(t) >= N / majorPrime and are 
%relatively prime to majorPrime. The minor primes are chosen heuristically so 
%that the the minor primes stay close to each other, do not grow much larger 
%than the associated majorPrime, and have a small sum. The specific rule 
%initially appeared in the implementation of DMSFT by Ruochuan Zhang, see 
%https://sourceforge.net/projects/aafftannarborfa/ . 
%
%If majorPrime is a vector, t will be a cell array of vectors of minor primes, 
%one which satisfies the above conditions for each element of majorPrime. For 
%consistency, if majorPrime has only one element, t will still be a cell array 
%containing a vector of minor primes.
%
%The following example suggests the usage of powers of primes for minor primes:
%Suppose that 300 = N / s for some major prime s. If we use successive primes 
%for the minor primes, we would obtain
%  2 * 3 * 5 * 7 * 11 > 300,
%meaning the minor primes sum to 28.
%However, if we use powers of primes, we can have
%  3^2 * 5 * 7 > 300,
%meaning the minor primes sum to only 21 and we only have three of them. This 
%reduces the number of rows in the measurement matrix as well as the sizes of 
%the FFTs used to generate it.
%
% Syntax:  [ t ] = minorPrimes(plan)
%     or:  [ t ] = plan.minorPrimes()
%
% Outputs:
%    t: A length K cell array of vectors of minor primes, satisfying the above 
%		conditions for each prime in majorPrime.
%
% See also: MAJORPRIMES

% Author: Craig Gross
% Mar 2020

% Make sure primeList is long enough
if plan.primeList(end) < max(2 * plan.s(end), 5)
	plan.primeList = primes(max(2 * plan.s(end), 5));
end

t = cell(1, length(plan.s));
for i = 1:length(t)
	% Start with t(1) = 2^1
	tBase = 2;
	alpha = 1;
	minorPrimeProduct = prod(tBase .^ alpha);
	% Keep track of the next prime to use as minor prime.
	nextPrime = 3; 
	while minorPrimeProduct < plan.N / plan.s(i)
		% To keep the minor primes close, we will only add a new prime for a 
		% base if it is smaller than (tBase .^ alpha) .* (tBase - 1). 
		% Otherwise, we will bump up the exponent corresponding to the 
		% smallest of these values. This ensures that as the loop progresses, 
		% the minorPrimeProduct grows slowly towards N / majorPrime(i).
		[minimalTestValue, idx] = min((tBase .^ alpha) .* (tBase - 1));
		if nextPrime < minimalTestValue
			tBase = [tBase, nextPrime]; %#ok<AGROW>
			alpha = [alpha, 1]; %#ok<AGROW>
			% Make sure primeList is long enough
			if length(plan.primeList) < length(tBase) + 2
				plan.primeList = primes(2 * plan.primeList(end));
			end
			% Make sure to skip the major prime
			if plan.primeList(length(tBase)+ 1) >= plan.s(i)
				nextPrime = plan.primeList(length(tBase) + 2);
			else
				nextPrime = plan.primeList(length(tBase) + 1);
			end
		else
			alpha(idx) = alpha(idx) + 1; %#ok<AGROW>
		end
		minorPrimeProduct = prod(tBase .^ alpha);
	end
	t{i} = tBase .^ alpha;
end

end
