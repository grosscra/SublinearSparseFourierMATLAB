function [ inverse ] = modularInverse(p, q)
%MODULARINVERSE- Calculates p^(-1) mod q for relatively prime integers p and q.
%Algorithm from https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm#Modular_integers.
%
% Syntax:  [ inverse ] = modularInverse(p, q)
%
% Inputs:
%    p: Integer.
%    q: Integer relatively prime to p.
%
% Outputs:
%    inverse: p^(-1) mod q
%
% Author: Craig Gross
% Mar 2020


inverse = 0; newInverse = 1;
r = q; newr = p;

while newr ~= 0
	quotient = floor(r / newr);
	temp = inverse;
	inverse = newInverse; newInverse = temp - quotient * newInverse;
	temp = r;
	r = newr; newr = temp - quotient * newr;
end

if r > 1
	error([num2str(p), ' is not invertible modulo ', num2str(q), '.']);
end
if inverse < 0
	inverse = inverse + q;
end

end
