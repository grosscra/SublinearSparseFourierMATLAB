function [ nodes ] = samplingNodes(plan)
%SAMPLINGNODES- Construct set of nodes for signal sampling.
%The sampling nodes consist of equispaced knots on [0, 1] with various mesh sizes. 
%In particular, we concatenate all meshes of size s(j) * t(i) (hence the use of 
%plan.st). These sampling nodes are then used for the FFTs in GENERALFASTMULTIPLY.
%
% Syntax:  [ nodes ] = samplingNodes(plan)
%      or  [ nodes ] = plan.samplingNodes()
%
% Outputs:
%    nodes: Column vector of sum(st) sampling nodes needed to construct the product 
%		of the measurement matrix with large Fourier transform of the signal.
%
% See also: GENERALFASTMULTIPLY

% Author: Craig Gross
% Mar 2020

nodes = zeros(plan.stSum(end), 1);
for j = 1:length(plan.st)
	stVal = plan.st(j);
	startIdx = plan.stSum(j);
	nodes(startIdx + 1: startIdx + stVal) = linspace(0, (stVal - 1) / stVal, stVal)';
end

end
