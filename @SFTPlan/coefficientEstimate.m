function [ coefficients, frequencies ] = coefficientEstimate(plan, product, identified)
%COEFFICIENTESTIMATE-Constructs 2k-sparse approximation of Fourier coefficients.
%Based on the frequencies corresponding to the most energetic Fourier 
%coefficients which have been identified more than length(s) / 2 times, from 
%the FREQUENCYIDENTIFICATION, approximations are constructed by taking 
%medians of the approximated values from the product matrix produced by 
%GENERALFASTMULTIPLY.
%
% Syntax:  [coefficients, frequencies] = coefficientEstimate(plan, product, identified)
%      or  [coefficients, frequencies] = plan.coefficientEstimate(product, identified)
%
% Inputs:
%    product: Extended row tensor product measurement matrix multiplied by 
%		various Fourier transforms of given signal. Should have form calculated 
%		by GENERALFASTMULTIPLY.
%    identified: An array of frequencies which have been identified using 
%		FREQUENCYIDENTIFICATION > K / 2 times.
%    k: Sparsity level.
%    weights: (Optional, Default = ones(1, K)) A length K row vector 
%		containing the weight given to each prime in s. This is only used for 
%		cases where there can be repeated major primes, e.g., when a random 
%		set is chosen with replacement (see RANDOMMAJORPRIMES). 
%
% Outputs:
%    coefficients: Approximations of the 2k most energetic Fourier coefficients.
%    frequencies: The 2k frequencies corresponding to the output coefficients.
%
% See also: GENERALFASTMULTIPLY, FREQUENCYIDENTIFICATION

% Author: Craig Gross
% Mar 2020

L = plan.sSum(end);
x = zeros(1, length(identified));

for j = 1:length(identified)
	omega = identified(j);
	% We find r_{j, omega mod s(j)} and \bar r_{i, j, omega mod s(j) * t{j}(i)} 
	% for all 1 <= j <= K, 1 <= i <= length(t{j}).
	% See GENERALFASTMULTIPLY for an explanation of the indexing scheme.
	tensorProductIndices = cell(1, plan.K);
	rbarBase = L;
	for i = 1:plan.K
		rbar = rbarBase + plan.s(i) * plan.tSum{i}(1:end - 1) + ...
			mod(omega, plan.s(i) * plan.t{i}) + 1;
		% Each prime should be counted weights(i) number of times
		tensorProductIndices{i} = repmat(rbar, 1, plan.weights(i));
		% We move into the part of product corresponding to s(i + 1)
		rbarBase = rbarBase + plan.s(i) * plan.tSum{i}(end);
	end
	tensorProductIndices= cell2mat(tensorProductIndices);
	indices = [repelem(plan.sSum(1:end - 1) + mod(omega, plan.s) + 1, plan.weights),...
		tensorProductIndices];
	x(j) = median(real(product(indices))) + 1i * median(imag(product(indices)));
end

% Take 2k largest coefficients
[coefficients, idx] = sort(x, 'descend');
coefficients = coefficients(1:min(2 * plan.k, length(x)));
frequencies = identified(idx(1:min(2 * plan.k, length(x))));

if length(x) < plan.k
	warning('SFTPlan:tooFewCoeffs',...
		'Only %d Fourier coefficients identified when the sparsity level was %d.',...
		length(x), plan.k);
end

if any((frequencies(1:min(plan.k, length(x))) <= -ceil(plan.N / 2)) |...
		(frequencies(1:min(plan.k, length(x))) > floor(plan.N / 2)))
	warning('SFTPlan:outsideBand',...
		'Significant frequencies outside the frequency band [%d, %d] were determined.',...
		-ceil(plan.N / 2) + 1, floor(plan.N / 2));
end

end
