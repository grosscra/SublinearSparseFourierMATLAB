function [ coefficients, frequencies ] = mexExecute(plan, samples)
%MEXEXECUTE- Constructs sparse Fourier approximation to a signal in mex
%Takes advantage of the precomputed plan information and the samples that must be 
%evaluated at the nodes in [0, 1] for the given plan to return a 2 * k sparse 
%approximation of the Fourier coefficients of a periodic function in the bandwidth N.
%
%This implementation uses a parallelized mex implementation of both the 
%frequency identification and coefficient estimation steps. This 
%implementation uses pocketfft and takes advantage of parallelization in 
%OpenMP.
%
% Syntax:  [ coefficients, frequencies ] = mexExecute(plan, samples)
%      or  [ coefficients, frequencies ] = plan.mexExecute(samples)
%
% Inputs:
%    samples: Either a column vector of function evaluations of the signal at the 
%		 sampling nodes in plan.nodes or a function handle. In the latter case, the 
%		 function will sample the function handle at plan.nodes. This function 
%		 handle must accept vectorized input.
%
% Outputs:
%    coefficients: Approximations of the 2k most energetic Fourier coefficients.
%    frequencies: The 2k frequencies corresponding to the output coefficients.
%		The frequencies are given by their canonical values in the interval
%		-ceil(N / 2) + 1:floor(N / 2). To obtain 0-indexed discrete 
%		frequencies with bandwidth N, one should apply mod(frequencies, N).
%
% Mex files required: parFreqIdentCoeffMex.mexa64
%
% See also: EXECUTE PAREXECUTE

% Author: Craig Gross
% Mar 2020
% MEX code by Toni Volkmer

if isa(samples, 'function_handle')
	samples = samples(plan.nodes);
end

if length(samples) ~= plan.stSum(end)
	error('Samples must have same length as the sampling nodes in the plan.');
end

nthreads = maxNumCompThreads;
omega_w = cell(1, plan.K);

s_j_times_tSum_j_end_array = zeros(plan.K,1);

for j = 1:plan.K
  s_j_times_tSum_j_end_array(j) = plan.s(j) * plan.tSum{j}(end);
  omega_w{j} = repmat(plan.weights(j), plan.s(j), 1);
end

% [product, omega] = SFTPlan.parFreqIdentMex(samples, plan.s, plan.sSum, plan.t, s_j_times_tSum_j_end_array, nthreads);
% 
% [frequencies, ~, indices] = unique(cell2mat(omega')');
% % timesIdentified = accumarray(indices, 1);
% timesIdentified = accumarray(indices,cell2mat(omega_w'));
% identified = frequencies(timesIdentified > sum(plan.weights) / 2);
% 
% % Coefficient estimate each frequency
% % x = zeros(1, length(identified));
% % for j = 1:length(identified)
% % 	x(j) = plan.coefficientForOneFreq(identified(j), product);
% % end
% x = SFTPlan.coefficientsForAllFreqMex(product, identified, plan.s, plan.t, plan.tSum, plan.weights, nthreads);

[identified,x] = SFTPlan.parFreqIdentCoeffMex(samples, plan.s, plan.sSum, plan.t, s_j_times_tSum_j_end_array, plan.tSum, plan.weights, nthreads);
% if length(intersect(id1,identified)) ~= length(identified)
%   error('mismatch');
% end
% if max(abs(x-x2)) > 0
%   error('mismatch2');
% end

% Take 2k largest coefficients
[coefficients, idx] = sort(x, 'descend');
coefficients = coefficients(1:min(2 * plan.k, length(x)));
frequencies = identified(idx(1:min(2 * plan.k, length(x))));

if length(x) < plan.k
	warning('SFTPlan:tooFewCoeffs',...
		'Only %d Fourier coefficients identified when the sparsity level was %d.',...
		length(x), plan.k);
end

if any((frequencies(1:min(plan.k, length(x))) <= -ceil(plan.N / 2)) |...
		(frequencies(1:min(plan.k, length(x))) > floor(plan.N / 2)))
	warning('SFTPlan:outsideBand',...
		'Significant frequencies outside the frequency band [%d, %d] were determined.',...
		-ceil(plan.N / 2) + 1, floor(plan.N / 2));
end

end
