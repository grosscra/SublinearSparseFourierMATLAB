function [ s ] = randomMajorPrimes(plan)
%RANDOMMAJORPRIMES- Constructs random list of major primes for meas. matrix.
%The candidate list of major primes is calulated so that the first candidate
%s(1) has a sequence of powers of primes smaller than s(1) such that their 
%product is at least N / s(1). The candidates are those sequentially 
%following the above calculated s(1). We determine 
%K = C * k * floor(log(N) / log(s(1))) + 1 
%total candidates as directed in [Iwen, 2013]. We then choose at random 
%l = ceil(randomScale * log(N / (1 - sigma)) values from this list of 
%candidates WITH replacement.
%
% Syntax:  [ s ] = randomMajorPrimes(plan)
%     or:  [ s ] = plan.randomMajorPrimes()
%
% Outputs:
%    s: Random row vector of l primes as in the description above.
%
% See also: MAJORPRIMES MINORPRIMES

% Author: Craig Gross
% Mar 2020

candidates = plan.majorPrimes();

l = ceil(plan.randomScale * log(plan.N / (1 - plan.sigma)));
K = length(candidates);
s = sort(candidates(randi(K, 1, l)));

end
