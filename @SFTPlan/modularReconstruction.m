function [ omega ] = modularReconstruction(r, s)
%MODULARRECONSTRUCTION- Calculate omega using only remainders r = omega mod s.
%We are guaranteed a unique 0 <= omega < prod(s) by the Chinese remainder 
%theorem. Note that we assume that the integers in s are relatively prime.
%
% Syntax:  [ omega ] = modularReconstruction(r, s)
%
% Inputs:
%    r: Row vector of remainders r = omega mod s satisfying 0 <= r < s.
%		Optionally, r can also be a matrix, where each column r(:, i) 
%		represents the remainders of different omega mod s(i), and the
%		function will simply be applied to all elements.
%    s: Vector of relatively prime positive integers.
%
% Outputs:
%    omega: The unique 0 <= omega < prod(s) satisfying omega = r mod s.
%		If the r is a matrix, omega is a column vector with as many rows as r,
%		with each entry having the corresponding remainders given by the 
%		columns of r.
%
% Other m-files required: modularInverse.m
% Subfunctions: reconstruct
%

% Author: Craig Gross
% Mar 2020
	
% Operate incrementally, starting from first two remainders and working up.
omega = r(:, 1);
divisor = s(1);
for i = 2:length(s)
	omega = reconstruct(omega, r(:, i), divisor, s(i));
	divisor = divisor * s(i);
end

end

function [ omega ] = reconstruct(r1, r2, s1, s2)
%RECONSTRUCT- Perform the modular reconstruction for only two remainders.
%The algorithm is justified by the following calculation:
% omega = r1 mod s1 = r2 mod s2
%	implies omega = r1 + s1 * j = r2 mod s2
%	implies j = s1^-1 (r2 - r1) mod s2
% Note that 0 <= r1 < s1 and 0 <= j < s2 
%	implies 0 <= omega = r1 + s1 * j < s1 * s2.
%
% Syntax:  function [ omega ] = reconstruct(r1, r2, s1, s2)
%
% Inputs:
%    r1: The remainder of omega mod s1. Can be column vector.
%    r2: The remainder of omega mod s2. Can be column vector of same length 
%		as r1.
%    s1: A positive integer.
%    s2: A positive integer relatively prime to s1.
%
% Outputs:
%    omega: The unique 0<=omega<s1*s2 satisfying omega = r1 mod s1 = r2 mod s2.
%		If the remainders are column vector, so is omega, with each entry 
%		having the corresponding remainders in r1 and r2.

% Author: Craig Gross
% Mar 2020
%
	omega = r1 + s1 .* mod(SFTPlan.modularInverse(s1, s2) .* (r2 - r1), s2);
end
