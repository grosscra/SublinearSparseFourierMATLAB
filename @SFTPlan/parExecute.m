function [coefficients, frequencies] = parExecute(plan, samples)
%PAREXECUTE- Construct sparse Fourier approximation to a signal in parallel
%Takes advantage of the precomputed plan information and the samples that must be 
%evaluated at the nodes in [0, 1] for the given plan to return a 2 * k sparse 
%approximation of the Fourier coefficients of a periodic function in the bandwidth N.
%
%This implementation of plan.execute uses MATLABs parfor to reconstruct 
%frequencies for each prime in parallel. The code then synchronizes, chooses 
%the most energetic frequencies, and estimates each coefficient for those 
%frequencies in a second parfor.
%
% Syntax:  [ coefficients, frequencies ] = parExecute(plan, samples)
%      or  [ coefficients, frequencies ] = plan.parExecute(samples)
%
% Inputs:
%    samples: A column vector of function evaluations of the signal at the 
%		 sampling nodes in plan.nodes 
%
% Outputs:
%    coefficients: Approximations of the 2k most energetic Fourier coefficients.
%    frequencies: The 2k frequencies corresponding to the output coefficients.
%		The frequencies are given by their canonical values in the interval
%		-ceil(N / 2) + 1:floor(N / 2). To obtain 0-indexed discrete 
%		frequencies with bandwidth N, one should apply mod(frequencies, N).
%
% Other m-files required: onePrime.m, coefficientForOneFreq.m
%
% See also: EXECUTE MEXEXECUTE

% Author: Craig Gross
% Apr 2020

product = cell(plan.K, 1);
omegas = cell(plan.K, 1);

% Take FFTs and identify frequencies in parallel
parfor j = 1:plan.K
	% Need to find samples for only prime s(j)
	r = plan.sSum(j) + (1:plan.s(j));
	% Now need samples for all products of s(j) with t{j}.
	% See GENERALFASTMULTIPLY for indexing scheme
	% Skip only major prime samplings
	rbar = plan.sSum(end);
	% Skip parts corresponding to previous major primes
	for jj = 1:j - 1
		rbar = rbar + plan.s(jj) .* plan.tSum{jj}(end);
	end
	% Now in s(j) part of samples. Cover all minor primes.
	rbar = rbar + (1:(plan.s(j) .* plan.tSum{j}(end)));
	[product{j}, omega{j}] = plan.onePrime(j, samples([r, rbar]));
end

[frequencies, ~, indices] = unique(cell2mat(omega')');
timesIdentified = accumarray(indices, 1);
identified = frequencies(timesIdentified > sum(plan.weights) / 2);

% Coefficient estimate each frequency
x = zeros(1, length(identified));
parfor j = 1:length(identified)
	x(j) = plan.coefficientForOneFreq(identified(j), product);
end

% Take 2k largest coefficients
[coefficients, idx] = sort(x, 'descend');
coefficients = coefficients(1:min(2 * plan.k, length(x)));
frequencies = identified(idx(1:min(2 * plan.k, length(x))));

if length(x) < plan.k
	warning('SFTPlan:tooFewCoeffs',...
		'Only %d Fourier coefficients identified when the sparsity level was %d.',...
		length(x), plan.k);
end

if any((frequencies(1:min(plan.k, length(x))) <= -ceil(plan.N / 2)) |...
		(frequencies(1:min(plan.k, length(x))) > floor(plan.N / 2)))
	warning('SFTPlan:outsideBand',...
		'Significant frequencies outside the frequency band [%d, %d] were determined.',...
		-ceil(plan.N / 2) + 1, floor(plan.N / 2));
end

end
