function [ identified ] = frequencyIdentification(plan, product)
%FREQUENCYIDENTIFICATION- Identify frequencies with largest coefficients.
%By comparing coefficients with large magnitudes which appear in the 
%constructed measurement matrix multiplied by the Fourier transform of the 
%signal, the frequencies which correspond to these largest or "most energetic" 
%frequencies are repeatedly identified. Only the ones that are identified 
%enough to be useful in a coefficient estimation step are kept.
%
% Syntax:  [ identified ] = frequencyIdentification(plan, product)
%      or  [ identified ] = plan.frequencyIdentification(product)
%
% Inputs:
%    product: Extended row tensor product measurement matrix multiplied by 
%		various Fourier transforms of given signal. Should have form calculated 
%		by GENERALFASTMULTIPLY.
%
% Outputs:
%    identified: Vector of reconstructed frequencies. Only frequencies which 
%		have been reconstructed more half total number of primes times will be 
%		returned.
%
% See also: GENERALFASTMULTIPLY

% Author: Craig Gross
% Mar 2020

L = plan.sSum(end);

identified = zeros(1, L);
timesIdentified = zeros(1, L);
numIdentified = 0;

% Below, we consider all residues h of each major prime s(j). If a 
% significant frequency = h mod s(j), we calculate its remainders modulo each 
% t{j}(i) by searching over these residues for the entry in the product 
% closest to the original entry corresponding to h mod s(j). The original 
% entry is indexed with r and the entries in the product corresponding to 
% residues of t{j}(i) are indexed with rbar. 
%
% See GENERALFASTMULTIPLY for explanation of indexing scheme.
rbarBase = L;
for j = 1:plan.K
	h = (0:plan.s(j) - 1)'; % All residues of s(j)
	r = plan.sSum(j) + h + 1; % Their locations in product
	divisor = plan.s(j) * prod(plan.t{j}); 
	remainders = zeros(plan.s(j), length(plan.t{j}));
	for i = 1:length(plan.t{j})
		% All pairs of residues of s(j) and residues of t{j}(i).
		rbar = rbarBase + plan.s(j) * (0:(plan.t{j}(i) - 1)) + h + 1;
		% Find the residues of t{j}(i) which match the original element best
		[~, bmin] = min( abs(product(r) - product(rbar)), [], 2 );
		remainders(:, i) = mod(plan.s(j) .* (bmin - 1) + h, plan.t{j}(i));

		% Move into the part of product corresponding to t{j}(i + 1)
		% or s(j + 1) if there are no more t{j} values.
		rbarBase = rbarBase + plan.s(j) * plan.t{j}(i);
	end
	omega = SFTPlan.modularReconstruction([h, remainders], ...
		[plan.s(j), plan.t{j}]);

	% Ensure frequencies are in the canonical interval
	% -ceil(divisor / 2) + 1:floor(divisor / 2)
	omega = mod(omega - 1 + ceil(divisor / 2), divisor) ...
		+ 1 - ceil(divisor / 2);

	% Track how many times each frequency has been found
	[~, omegaMatch, idMatch] = intersect(omega, identified(1:numIdentified));
	% Frequencies already found
	% For repeated primes, we count this identifications weights(j) times
	timesIdentified(idMatch) = timesIdentified(idMatch) + plan.weights(j);
	% Frequencies to add to list
	omegaNotMatch = setdiff(1:plan.s(j), omegaMatch);
	identified(numIdentified+1:numIdentified+length(omegaNotMatch)) =...
		omega(omegaNotMatch);
	% For repeated primes, we count this identifications weights(j) times
	timesIdentified(numIdentified+1:numIdentified+length(omegaNotMatch))=...
		plan.weights(j);
	numIdentified = numIdentified + length(omegaNotMatch);
end

% Need to be identified more than the TOTAL number of primes including repeats
identified = identified(timesIdentified > sum(plan.weights) / 2);

end
