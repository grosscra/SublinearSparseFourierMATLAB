# Replace with your MATLAB bin directory containing mex and mexext as necessary
MATLABROOT=$(shell matlab -e | grep -P -o '(?<=^MATLAB=).+')
MEXBIN=$(MATLABROOT)/bin

POCKETFFT=Lib/pocketfft

# WARNING: Use gcc version < 9.3.0, e.g., on Ubuntu 20.04, install old version and c++ library with:
# apt install gcc-7 libstdc++-7-dev
# and use:
# CC=gcc-7 -fopenmp
CC=gcc -fopenmp

CFLAGS=-I$(POCKETFFT)
COPTIMFLAGS=-O3 -march=native -ffast-math
LDOPTIMFLAGS=

OUTDIR=@SFTPlan
MEXCODEDIR=MexCode

MEXFLAGS=-R2018a -outdir $(OUTDIR)
MEX=$(MEXBIN)/mex
MEXEXT=$(shell $(MEXBIN)/mexext)

.PHONY: all clean

all: $(OUTDIR)/parFreqIdentMex.$(MEXEXT) $(OUTDIR)/coefficientsForAllFreqMex.$(MEXEXT) $(OUTDIR)/parFreqIdentCoeffMex.$(MEXEXT)

$(OUTDIR)/parFreqIdentMex.$(MEXEXT): $(MEXCODEDIR)/parFreqIdentMex.c $(POCKETFFT)/pocketfft.c
	$(MEX) CC='$(CC)' COPTIMFLAGS='$(COPTIMFLAGS)' LDOPTIMFLAGS='$(LDOPTIMFLAGS)' $(MEXFLAGS) $(CFLAGS) $(MEXCODEDIR)/parFreqIdentMex.c $(POCKETFFT)/pocketfft.c

$(OUTDIR)/coefficientsForAllFreqMex.$(MEXEXT): $(MEXCODEDIR)/coefficientsForAllFreqMex.c
	$(MEX) CC='$(CC)' COPTIMFLAGS='$(COPTIMFLAGS)' LDOPTIMFLAGS='$(LDOPTIMFLAGS)' $(MEXFLAGS) $(CFLAGS) $(MEXCODEDIR)/coefficientsForAllFreqMex.c

$(OUTDIR)/parFreqIdentCoeffMex.$(MEXEXT): $(MEXCODEDIR)/parFreqIdentCoeffMex.c $(POCKETFFT)/pocketfft_prealloc.c
	$(MEX) CC='$(CC)' COPTIMFLAGS='$(COPTIMFLAGS)' LDOPTIMFLAGS='$(LDOPTIMFLAGS)' $(MEXFLAGS) $(CFLAGS) $(MEXCODEDIR)/parFreqIdentCoeffMex.c $(POCKETFFT)/pocketfft_prealloc.c


clean:
	rm -f "$(OUTDIR)/*.$(MEXEXT)"
