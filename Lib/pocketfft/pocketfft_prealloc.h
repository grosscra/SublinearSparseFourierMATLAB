/*
 * This file is part of pocketfft.
 * Licensed under a 3-clause BSD style license - see LICENSE.md
 */

/*! \file pocketfft.h
 *  Public interface of the pocketfft library
 *
 *  Copyright (C) 2008-2018 Max-Planck-Society
 *  \author Martin Reinecke
 */

#ifndef POCKETFFT_H
#define POCKETFFT_H

#include <stdlib.h>

typedef struct cmplx {
  double r,i;
} cmplx;

#define NFCT 25
typedef struct cfftp_fctdata
  {
  size_t fct;
  cmplx *tw, *tws;
  } cfftp_fctdata;

typedef struct cfftp_plan_i
  {
  size_t length, nfct;
  cmplx *mem;
  cfftp_fctdata fct[NFCT];
  } cfftp_plan_i;
typedef struct cfftp_plan_i * cfftp_plan;

typedef struct fftblue_plan_i
  {
  size_t n, n2;
  cfftp_plan_i plan;
  double *mem;
  double *bk, *bkf;
  } fftblue_plan_i;
typedef struct fftblue_plan_i * fftblue_plan;

// struct cfft_plan_prealloc_i;
typedef struct cfft_plan_prealloc_i
  {
  cfftp_plan_i packplan;
  fftblue_plan_i blueplan;
  int use_plan;
  } cfft_plan_prealloc_i;
typedef struct cfft_plan_prealloc_i * cfft_plan_prealloc;
int make_cfft_plan_prealloc (cfft_plan_prealloc plan, size_t length, double *mem_twsize, double *twid_c2length, double *ch_2cfftlength, double *wal_2ip, double *mem_blue);
void destroy_cfft_plan_prealloc (cfft_plan_prealloc plan);
int cfft_backward_prealloc(cfft_plan_prealloc plan, double c[], double fct, double *twid_c2length, double *ch_2cfftlength, double *wal_2ip);
int cfft_forward_prealloc(cfft_plan_prealloc plan, double c[], double fct, double *twid_c2length, double *ch_2cfftlength, double *wal_2ip);
size_t cfft_length_prealloc(cfft_plan_prealloc plan);

#endif
