%TESTPLANPRIMEREDUCTION- Tests SFTPlan and its primes.
%
% Classes required: SFTPlan
%
% See also: SFTPlan

addpath("..");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

N = 2^19;
k = 100;
plan = SFTPlan(N, k, 'deterministic', 'C', 0.015, 'primeList', ...
	primeList, 'primeShift', 35);

trueFreq = -ceil(N / 2) + randperm(N, k);
trueCoeff = exp(2i * pi * rand(1, k));
samples = plan.sampleTrigPolynomial(trueCoeff, mod(trueFreq, N));

tic
[approxCoeff, approxFreq] = plan.execute(samples);
toc

numCorrect = length(intersect(trueFreq, approxFreq));
if numCorrect ~= k
	fprintf('Only %d out of %d frequencies recovered', numCorrect, k);
	disp(intersect(trueFreq, approxFreq));
end
