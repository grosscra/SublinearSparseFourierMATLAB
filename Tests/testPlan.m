%TESTPLAN- Tests SFTPlan with some simple polynomials.
%
% Classes required: SFTPlan
% Other m-files required: trigPoly.m
%
% See also: SFTPlan

addpath("..");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

% Test constant function

N = 100;
k = 1;
plan = SFTPlan(N, k, 'deterministic', 'primeList', primeList);

fHat = zeros(N, 1);
fHat(ceil(N / 2)) = 1 + 1i; % Matches zero frequency for use in trigPoly
f = @(x) trigPoly(x, fHat);

% User sampling
samples = f(plan.nodes);
[coefficients, frequencies] = plan.execute(samples);

assert(any(frequencies == 0));
assert(norm(coefficients(frequencies == 0) - (1 + 1i)) < 1e-8);
disp('Constant function with user sampling test passed');

% Auto-sampling
[coefficients, frequencies] = plan.execute(f);

assert(any(frequencies == 0));
assert(norm(coefficients(frequencies == 0) - (1 + 1i)) < 1e-8);
disp('Constant function with auto-sampling test passed');

% Test one negative frequency

N = 80;
k = 1;
plan = SFTPlan(N, k, 'deterministic', 'primeList', primeList);

trueCoeff = [1 + 1i];
trueFreq = [-1];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

% User sampling
samples = f(plan.nodes);
[coefficients, frequencies] = plan.execute(samples);

assert(any(frequencies == -1));
assert(norm(coefficients(frequencies == -1) - (1 + 1i)) < 1e-8);
disp('One negative frequency with user sampling test passed');

% Auto-sampling
[coefficients, frequencies] = plan.execute(f);

assert(any(frequencies == -1));
assert(norm(coefficients(frequencies == -1) - (1 + 1i)) < 1e-8);
disp('One negative frequency with auto-sampling test passed');

% Test function with two small frequencies

N = 33;
k = 2;
plan = SFTPlan(N, k, 'deterministic', 'primeList', primeList, 'C', 8);

trueCoeff = [exp(2i * pi * (1/8)), exp(2i * pi * (2/3))];
trueFreq = [0, 1];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

% User sampling
samples = f(plan.nodes);
[coefficients, frequencies] = plan.execute(samples);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == k);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two small frequencies with user sampling test passed');

% Auto-sampling
[coefficients, frequencies] = plan.execute(f);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == k);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two small frequencies with auto-sampling test passed');

% Test function with two small non-positive frequencies

trueCoeff = [exp(2i * pi * (1/8)), exp(2i * pi * (2/3))];
trueFreq = [-1, -2];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

% User sampling
samples = f(plan.nodes);
[coefficients, frequencies] = plan.execute(samples);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == k);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two small nonpositive frequencies with user sampling test passed');

% Auto sampling
[coefficients, frequencies] = plan.execute(f);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == k);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two small nonpositive frequencies with auto-sampling test passed');

% Test function with two mixed sign frequencies

trueCoeff = [exp(2i * pi * 13), exp(2i * pi * 74)];
trueFreq = [3, -16];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

% User sampling
samples = f(plan.nodes);
[coefficients, frequencies] = plan.execute(samples);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == k);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two mixed sign frequencies with user sampling test passed');

% Auto sampling
[coefficients, frequencies] = plan.execute(f);

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == k);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Two mixed sign frequencies with auto-sampling test passed');

% Test outside of frequency band
N = 128;
k = 1;
plan = SFTPlan(N, k, 'deterministic', 'primeList', primeList, 'C', 50);

trueCoeff = [exp(2i * pi * (2/3))];
trueFreq = [200];
f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

% User sampling
samples = f(plan.nodes);
[coefficients, frequencies] = plan.execute(samples)

[recovered, locations] = ismembertol(trueFreq, frequencies);
assert(nnz(recovered) == k);
assert(norm(trueCoeff - coefficients(locations)) < 1e-8);
disp('Frequency outside band with user sampling test passed');
