%TESTRANDOMPLANRANDOMPOLYNOMIALSLARGEN- Tests random SFTPlan with large N. 
%Compares parallel execution wiht various threads and saves results to file.
%
% Classes required: SFTPlan
%
% See also: SFTPlan

% Author: Craig Gross
% Mar 2020
% Modified: Toni Volkmer

addpath("..");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

fileID = 1; % Print to standard output
% Set output file as desired
% fileID = fopen('../output.txt', 'w');

numTrials = 10;
tol = 1e-6;
rng('default');
for n = 27
    N = 2^n;
    fprintf(fileID, 'N = 2^%d, numTrials = %d\n', n, numTrials);
    for l = [5]
        k = 2.^l;
        for nThreads = [0 1 4 8 16]
            if nThreads == 0
                fprintf(fileID, '-------\nexecute:\n');
                maxNumCompThreads('automatic');
            else
                maxNumCompThreads(nThreads);
            end
            
            timerTotal = tic;
            tSampling = 0;
            
            failures = 0;
            % Pushing coefficients to limits
            plan = SFTPlan(N, k, 'random', 'primeList', primeList, 'C', 0.5, 'sigma', 0.01);
            for i = 1:numTrials
                trueFreq = -ceil(N / 2) + randperm(N, k);
                trueCoeff = exp(2i * pi * rand(1, k));
                f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
                timerSampling = tic;
                samples = plan.sampleTrigPolynomial(trueCoeff, trueFreq);
                tSampling = tSampling + toc(timerSampling);
                if nThreads == 0
                    [approxCoeff, approxFreq] = plan.execute(samples);
                else
                    [approxCoeff, approxFreq] = plan.mexExecute(samples);
                end

                [recovered, locations] = ismembertol(trueFreq, approxFreq);
                if nnz(recovered) ~= k
                    disp('Not all frequencies recovered.');
                    failures = failures + 1;
                elseif norm(trueCoeff - approxCoeff(locations)) > tol
                    fprintf('Frequencies recovered, but L^2 error in coefficients surpasses %e\n',...
                        tol);
                    failures = failures + 1;
                end
            end
            if failures > 0
                fprintf(fileID, 'For N = %d, k = %d, nodes/N = %.2f, out of %d trials, %d failed.\n', N, k,...
                    length(plan.nodes)/N, numTrials, failures);
            end
            tTotal = toc(timerTotal);
            if nThreads == 0
                fprintf(fileID, 'k = 2^%d, %.3f seconds excluding sampling\n', l, tTotal - tSampling);
                fprintf(fileID, '-------\nmexExecute:\n');
                executeTime = tTotal - tSampling;
            else
                fprintf(fileID, 'nThreads = %d, k = 2^%d, %.3f seconds excluding sampling, %1.3f speedup\n', nThreads, l, tTotal - tSampling, executeTime ./ (tTotal - tSampling));
            end
        end
    end
end
if fileID >= 3
	fclose(fileID);
end
