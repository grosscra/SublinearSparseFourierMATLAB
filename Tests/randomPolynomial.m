function [f, coefficients, frequencies] = randomPolynomial(N, k) 
%RANDOMPOLYNOMIAL-Constructs a random trigonometric polynomial.
%
% Syntax:  [ t ] = randomPolynomial(k, N)
%
% Inputs:
%    N: Bandwidth.
%    k: Sparsity.
%
% Outputs:
%    f: Function handle to polynomial.
%    coefficients: Vector of k Fourier coefficients uniformly 
%		distributed around the unit circle.
%    frequencies: Vector of k frequencies corresponding to the 
%		given Fourier coefficients, subset of -ceil(N/2)+1:floor(N/2).
%		See below and the documentation for TRIGPOLY for further 
%		details on the frequency convention.
%
% Other m-files required: trigPoly.m
%
% See also: TRIGPOLY

coefficients = exp(2i * pi * rand(k, 1));
frequencyIndices= sort(randperm(N, k));
% Our canonical choice of frequencies is for them to be in
% -ceil(N / 2) + 1:floor(N / 2). The frequencies that will 
% eventually be output by fourierApproximate for example will be 
% equal to frequencyIndices mod N. We use frequencyIndices to 
% setup the trigonometric polynomial, but shift them to the 
% canoncial values and return them.
frequencies = mod(frequencyIndices - 1 + ceil(N/2), N) ...
	+ 1 - ceil(N/2);

% Shifting using the method in trigPoly documentation.
fHat = zeros(N, 1);
fHat(frequencyIndices) = coefficients;
f = @(x)trigPoly(x, fftshift(fHat));
	
end
