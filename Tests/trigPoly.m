function f = trigPoly(x, fHat) 
%TRIGPOLY- Evaluates trigonometric polynomial specified by coefficients
%The trigonometric polynomial is periodic on [0, 1]
%
% Syntax:  function f = trigPoly(x, fHat) 
%
% Inputs:
%    x: Column vector of n points to evaluate polynomial at.
%    fHat: Length N column vector representing Fourier coefficients. 
%
%		NOTE: The indexing for the Fourier coefficients is important:
%		The first entry corresponds to frequency -ceil(N / 2) + 1, the 
%		last entry corresponds to frequency floor(N / 2). The zero 
%		frequency is at entry ceil(N / 2).
%		
%		This function is INCOMPATIBLE with MATLAB's FFT convention 
%		out of the box. For example, a polynomial with bandwidth 6 
%		with Fourier coefficients 1, 2, and 3 corresponding to omega 
%		= -1, 0, 3 respectively should be input to trigPoly as
%			[0; 1; 2; 0; 0; 3].
%		If these Fourier coefficients were the result of a size 6 
%		MATLAB fft for example (where the coefficient for the zero 
%		frequency is in the first entry), one would obtain the vector
%			[2; 0; 0; 3; 0; 1].
%		Note that even applying fftshift to this vector is still 
%		incompatible:
%			fftshift(...) = [3; 0; 1; 2; 0; 0].
%		To apply fftshift and have a vector compatible with this 
%		function, the MATLAB index should correspond exactly to the 
%		frequency (mod N), that is, the zero frequency should be at 
%		the last index. Indeed,
%			fftshift([0; 0; 3; 0; 1; 2]) = [0; 1; 2; 0; 0; 3].
%
% Outputs:
%    f: Column vector of n function values at the given x-values.


% Author: Craig Gross
% Mar 2020

N = length(fHat);
f = exp(2i .* pi .* x * (-ceil(N / 2) + 1:floor(N / 2))) * fHat;

end
