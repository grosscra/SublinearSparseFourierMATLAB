%TESTTRIGPOLYSAMPLING- Tests sampling trig poly.
%
% Classes required: SFTPlan
%
% See also: SFTPlan

addpath("..");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

numTrials = 5;
tol = 1e-12;

for N = 2 .^ (10:15)
	for k = 2 .^ (3:min(log2(N), 5))
		for planType = ["deterministic", "random"]
			failures = 0;
			plan = SFTPlan(N, k, planType, 'primeList', primeList);
			ifftTime = 0;
			standardTime = 0;
			for i = 1:numTrials
				trueFreq = -ceil(N / 2) + randperm(N, k);
				trueCoeff = exp(2i * pi * rand(1, k));
				f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
				tic;
				samples1 = plan.sampleTrigPolynomial(trueCoeff, trueFreq);
				ifftTime = ifftTime + toc;
				%tic;
				%samples2 = f(plan.nodes);
				%standardTime = standardTime + toc;
				%if any(abs(samples1 - samples2) > tol)
				%	failures = failures + 1;
				%end
			end
			fprintf('For N = %d, k = %d, out of %d %s plan trials, %d failed.\n', N, k,...
				numTrials, planType, failures);
			fprintf('Took %4.5f seconds to sample via IFFT in total.\n', ifftTime);
			fprintf('Took %4.5f seconds to sample via function in total.\n\n', standardTime);
		end
	end
end

