%TESTRANDOMPLANRANDOMPOLYNOMIALSLARGEN- Tests random SFTPlan with large N. 
%
% Classes required: SFTPlan
%
% See also: SFTPlan

% Author: Craig Gross
% Mar 2020

addpath("..");

% Start new local parpool
%p = gcp('nocreate');
%if ~isempty(p)
%	delete(p);
%end
%p = parpool(2);

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

numTrials = 5;
tol = 1e-3;

% Serial
for N = 2 .^ 27
	for k = 2 .^ 5
		serialFailures = 0;
		parallelFailures = 0;
		fftFailures = 0;

		% Pushing coefficients to limits
		tic;
		serialPlan = SFTPlan(N, k, 'random', 'primeList', primeList, 'C', 0.5, 'sigma', 0.01);
		serialTime = toc;

		tic;
		parallelPlan = SFTPlan(N, k, 'random', 'primeList', primeList, 'C', 0.5, 'sigma', 0.01);
		parallelTime = toc;
		
		fftTime = 0;

		for i = 1:numTrials
			% Function setup and sampling
			trueFreq = -ceil(N / 2) + randperm(N, k);
			trueCoeff = exp(2i * pi * rand(1, k));
			f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';

			serialSamples = f(serialPlan.nodes);
			parallelSamples = f(parallelPlan.nodes);
			fftSamples = zeros(N, 1);
			parfor i = 0:N - 1
				fftSamples(i + 1) = f(i / N);
			end

			% Serial
			tic;
			[approxCoeff, approxFreq] = serialPlan.execute(serialSamples);
			serialTime = serialTime + toc;
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			if nnz(recovered) ~= k
				serialFailures = serialFailures + 1;
			elseif norm(trueCoeff - approxCoeff(locations)) > tol
				serialFailures = serialFailures + 1;
			end

			% Parallel
			tic;
			[approxCoeff, approxFreq] = parallelPlan.parExecute(parallelSamples);
			parallelTime = parallelTime + toc;
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			if nnz(recovered) ~= k
				parallelFailures = parallelFailures + 1;
			elseif norm(trueCoeff - approxCoeff(locations)) > tol
				parallelFailures = parallelFailures + 1;
			end

			% FFT
			tic;
			fHat = (1 ./ N) .* fft(fftSamples);
			fftTime = fftTime + toc;
			fHat = fftshift(fHat);
			[~, idx] = sort(fHat, 'descend');
			approxCoeff = fHat(idx(1:k)).';
			approxFreq = idx(1:k) - (ceil(N / 2) + 1);
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			if nnz(recovered) ~= k
				fftFailures = fftFailures + 1;
			elseif norm(trueCoeff - approxCoeff(locations)) > tol
				fftFailures = fftFailures + 1;
			end
		end

		fprintf('Serial: #samples / N = %3.4f.\n', length(serialSamples) ./ N);
		fprintf('Serial: For N = %d, k = %d, out of %d trials, %d failed.\n', N, k,...
			numTrials, serialFailures)
		fprintf('Serial: %5.6f seconds for %d trials.\n\n', serialTime, numTrials);

		fprintf('Parallel: #samples / N = %3.4f.\n', length(parallelSamples) ./ N);
		fprintf('Parallel: For N = %d, k = %d, out of %d trials, %d failed.\n', N, k,...
			numTrials, parallelFailures)
		fprintf('Parallel: %5.6f seconds for %d trials.\n\n', parallelTime, numTrials);

		fprintf('FFT: #samples / N = %3.4f.\n', length(fftSamples) ./ N);
		fprintf('FFT: For N = %d, k = %d, out of %d trials, %d failed.\n', N, k,...
			numTrials, fftFailures)
		fprintf('FFT: %5.6f seconds for %d trials.\n\n', fftTime, numTrials);
	end
end
