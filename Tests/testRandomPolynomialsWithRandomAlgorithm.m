%TESTRANDOMPOLYNOMIALS- Tests RANDOMFOURIERAPPROXIMATE with random 
%trigonometric polynomials of various bandwidths and sparsities.
%
% Other m-files required: randomFourierApproximate.m, 
%
% See also: RANDOMFOURIERAPPROXIMATE

% Author: Craig Gross
% Mar 2020

addpath("../Functions");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

numTrials = 10;
tol = 1e-8;

for N = 2 .^ (4:10)
	for k = 1:ceil(N / 2)
		failures = 0;
		for i = 1:numTrials
			trueFreq = -ceil(N / 2) + randperm(N, k);
			trueCoeff = exp(2i * pi * rand(1, k));
			f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
			[approxCoeff, approxFreq] = randomFourierApproximate(f, N, k,...
				'primeList', primeList, 'C', 8, 'sigma', 2/3);
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			if ~(sum(recovered) == k)
				disp('Not all frequencies recovered.');
				failures = failures + 1;
				% Retry
				[approxCoeff, approxFreq] = randomFourierApproximate(f, N, k,...
					'primeList', primeList, 'C', 14, 'sigma', 0.99);
				[recovered, locations] = ismembertol(trueFreq, approxFreq);
				if sum(recovered) == k
					fprintf('\tFound all on random retry\n');
				else
					fprintf('\tDid not find all on random retry\n');
					% Deterministic retry
					[approxCoeff, approxFreq] = fourierApproximate(f, N, k,...
						'primeList', primeList, 'C', 8);
					[recovered, locations] = ismembertol(trueFreq, approxFreq);
					if sum(recovered) == k
						fprintf('\t\tFound all on deterministic retry\n');
					else
						fprintf('\t\tDid not find all on deterministic retry\n');
					end
				end
			elseif norm(trueCoeff - approxCoeff(locations)) > tol
				fprintf('Frequencies recovered, but L^2 error in coefficients surpasses %e\n',...
					tol);
				failures = failures + 1;
			end
		end
		fprintf('For N = %d, k = %d, out of %d trials, %d failed.\n\n', N, k,...
			numTrials, failures)
	end
end
