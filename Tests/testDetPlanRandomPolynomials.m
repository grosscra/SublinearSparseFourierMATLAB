%TESTDETPLANRANDOMPOLYNOMIALS- Tests deterministic SFTPlan with random poly.
%
% Classes required: SFTPlan
%
% See also: SFTPlan

% Author: Craig Gross
% Mar 2020

addpath("..");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

numTrials = 5;

for N = 2 .^ (4:10)
	for k = 1:ceil(N / 4)
		plan = SFTPlan(N, k, 'deterministic', 'primeList', primeList);
		for i = 1:numTrials
			trueFreq = -ceil(N / 2) + randperm(N, k);
			trueCoeff = exp(2i * pi * rand(1, k));
			f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
			samples = f(plan.nodes);
			[approxCoeff, approxFreq] = plan.execute(samples);
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			assert(nnz(recovered) == k);
			assert(norm(trueCoeff - approxCoeff(locations)) < 1e-8)
		end
		fprintf('Passed test with N = %d, k = %d, %d times.\n', N, k, numTrials);
	end
end

