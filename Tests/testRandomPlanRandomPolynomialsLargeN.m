%TESTRANDOMPLANRANDOMPOLYNOMIALSLARGEN- Tests random SFTPlan with large N. 
%
% Classes required: SFTPlan
%
% See also: SFTPlan

% Author: Craig Gross
% Mar 2020

addpath("..");

if ~exist('primeList') || length(primeList) ~= 10000
	primeList = primes(104740); % First 10,000 primes
end

numTrials = 5;
tol = 1e-8;

for N = 2 .^ (16:2:26)
	for k = 1:ceil(N / 2^13)
		failures = 0;
		% Pushing coefficients to limits
		plan = SFTPlan(N, k, 'random', 'primeList', primeList, 'C', 0.5, 'sigma', 0.01);
		for i = 1:numTrials
			trueFreq = -ceil(N / 2) + randperm(N, k);
			trueCoeff = exp(2i * pi * rand(1, k));
			f = @(x) exp(2i .* pi .* x * trueFreq) * trueCoeff.';
			samples = f(plan.nodes);
			[approxCoeff, approxFreq] = plan.execute(samples);
			[recovered, locations] = ismembertol(trueFreq, approxFreq);
			if nnz(recovered) ~= k
				disp('Not all frequencies recovered.');
				failures = failures + 1;
			elseif norm(trueCoeff - approxCoeff(locations)) > tol
				fprintf('Frequencies recovered, but L^2 error in coefficients surpasses %e\n',...
					tol);
				failures = failures + 1;
			end
		end
		fprintf('For N = %d, k = %d, out of %d trials, %d failed.\n\n', N, k,...
			numTrials, failures)
	end
end
