function [ coefficients, frequencies ] = fourierApproximate(f, N, k, varargin)
%FOURIERAPPROXIMATE- Constructs sparse Fourier approximation of a function f
%Function uses Algorithm 3: Fourier Approximate 2 from [Iwen, 2013] to 
%construct a 2 * k sparse approximation of the Fourier coefficients of a 
%periodic function f contained in the bandwidth N.
%
%This function is just a wrapper for a one-off initialization/execution of 
%a deterministic SFTPlan.
%
% Syntax:  [ coefficients, frequencies ] = fourierApproximate(f, N, k, ...)
%     with optional Name, Value pairs for:
%		C (e.g., fourierApproximate(f, N, k, 'C', 4))
%		primeList (e.g., fourierApproximate(f, N, k, 'primeList', primeList))
%     or any combination of these Name, Value pairs.
%
% Inputs:
%    f: Function handle to signal.
%    N: Bandwidth.
%    k: Sparsity level.
%    primeList: (Optional) A list of sequential primes in row vector starting 
%		at 2. Precomputation speeds up multiple calls.
%    C: (Optional, Default = 8) A scaling parameter for the number of primes 
%		used to construct the measurement matrix. See SFTPlan.majorPrimes for 
%		details.
%
% Outputs:
%    coefficients: Approximations of the 2k most energetic Fourier coefficients.
%    frequencies: The 2k frequencies corresponding to the output coefficients.
%		The frequencies are given by their canonical values in the interval
%		-ceil(N / 2) + 1:floor(N / 2). To obtain 0-indexed discrete 
%		frequencies with bandwidth N, one should apply mod(frequencies, N).
%
% Other classes required: SFTPlan
%
% See also: SFTPlan RANDOMFOURIERAPPROXIMATE

% Author: Craig Gross
% Mar 2020

plan = SFTPlan(N, k, 'deterministic', varargin{:});
[coefficients, frequencies] = plan.execute(f);

end
